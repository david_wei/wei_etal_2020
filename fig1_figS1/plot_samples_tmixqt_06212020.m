function plot_samples_tmixqt_06212020(fname,fname_profile,n_start,n_end,varargin)
syms x
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
INVes(x)=finverse(es(x));
time=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','time');
time_bounds=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','time_bounds');
LH_baebbr_arm=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','LH_baebbr');
LH_qcecor_arm=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','LH_qcecor');
T_p=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','T_p');
T_sfc=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','T_sfc');
Td_p=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','Td_p');
rh_sfc=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','rh_sfc');
p=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','p');
p_sfc=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','p_sfc');
Td_sfc=double(vpa(INVes(es(T_sfc).*rh_sfc/100),3));
hour=ncread(fname,'hour');
lat=ncread(fname,'lat');
lon=ncread(fname,'lon');
gps_alt=ncread(fname,'gps_alt');
pres=ncread(fname,'pres');
vert_wind=ncread(fname,'vert_wind');
vert_wind(vert_wind<-100)=nan;
aircraft_east_vel=ncread(fname,...
    'aircraft_east_vel');
aircraft_north_vel=ncread(fname,...
    'aircraft_north_vel');
lwc_gerber=ncread(fname,'lwc_gerber');
specific_humid=ncread(fname,'specific_humid');
wvmr_dlh=ncread(fname,'wvmr_dlh');
temperature=ncread(fname,'temperature');
equi_potential_temp=ncread(fname,...
    'equi_potential_temp');
rh=ncread(fname,'rh');
rh=wvmr_dlh/1000./(es(temperature+273.15)./pres/100*18/29);
rho=(pres*100/287./(temperature+273.15));
cond=vert_wind>-100;
rs=min(es(temperature+273.15)/100./pres*18/29,wvmr_dlh/1000);
% r_s=wvmr_dlh/1000;
% r_s=min(es(temperature+273.15)/100./pres*18/29,wvmr_dlh/1000.*(1+...
%     (randn(size(wvmr_dlh))-0.5)*0.5));
equi_potential_temp3=equi_pot(temperature+273.15,pres,rs);
% Highest triangle, run after CHECKlevel*.m

trange=zeros(size(hour));
trange(n_start:n_end)=1;

alpha=0.272;
V_max=10;
tt=3402;% time for Radiosonde profile
p_leg=squeeze(mean(pres(trange==1)));% for leg 1
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));

% plot(equi_pot(T_sfc(tt),p_sfc(tt),...
%     es(T_sfc(tt))/100/p_sfc(tt)*18/29*rh_sfc(tt)/100)*alpha+...
%     (1-alpha)*(T_sfc(tt)*(1000/p_sfc(tt))^(287/1004)),...
%     es(T_sfc(tt))/100/p_sfc(tt)*18/29*rh_sfc(tt)/100,'bo','MarkerSize',6)
% xlim([310,320])
% ylim([4e-3,7e-3])
hold on
axis ij

% tdry_sonde=ncread('sgpsondewnpnC1.b1.20090522.113000.nc','tdry');
% rh_sonde=ncread('sgpsondewnpnC1.b1.20090522.113000.nc','rh');
% pres_sonde=ncread('sgpsondewnpnC1.b1.20090522.113000.nc','pres');
% r_sonde=es(tdry_sonde+273.15)/100./pres_sonde*18/29.*rh_sonde/100;
% equi1_sonde=equi_pot(tdry_sonde+273.15,pres_sonde,r_sonde);
% plot(equi1_sonde*alpha+(1-alpha)*(tdry_sonde+273.15).*...
%     (1000./pres_sonde).^(287/1004),r_sonde,'r')

% equi_leg=interp1(pres_sonde,equi1_sonde*alpha+...
%     (1-alpha)*(tdry_sonde+273.15).*(1000./pres_sonde).^(287/1004),p_leg);
% r_leg=interp1(pres_sonde,r_sonde,p_leg);
% plot(equi_leg,r_leg,'ro','MarkerSize',6)

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
syms x;
INVes(x)=finverse(es(x));
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alpha+(1-alpha)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),(0:0.1e-3:20e-3)*1000,'k')

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
cond=wvmr_dlh>0&lwc_gerber>0.1;
cond=wvmr_dlh>0&lwc_gerber>-0.01;
cond=cond&trange;
axis ij
hold on
tmp1=vert_wind(cond);
bluewhitered
Tmix=equi_potential_temp3*alpha+...
    (1-alpha)*((temperature+273.15).*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*lwc_gerber/1000./rho);
% scatter(Tmix(cond),...
%     (rs(cond)+lwc_gerber(cond)/1000./rho(cond))*1000,10,...
%     cmap(min(101,max(1,int64(tmp1/V_max*50+6*sign(tmp1)+51))),:),'filled')
scatter(Tmix(cond),...
    (rs(cond)+lwc_gerber(cond)/1000./rho(cond))*1000,10,'k','filled',...
    'MarkerFaceAlpha',0.2,'MarkerEdgeAlpha',0.2)

tmp=hist3([Tmix(cond),...
    rs(cond)+lwc_gerber(cond)/1000./rho(cond)],...
    'ctrs',{281:0.1:340,0:0.1e-3:20e-3})';
pertmp=arrayfun(@(x)sum(tmp(tmp>x))/sum(tmp(:)),tmp);
% [C,h]=contour(281:0.1:340,(0:0.1e-3:20e-3)*1000,pertmp,1,'k');
% xlim([306.5,313])
axis ij
% clabel(C,h)


T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);

for Tv_tmp=Tv_leg*[1]
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alpha+(1-alpha)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3)*1000,'k--')
end

xlabel([num2str(alpha),'\theta_{e}+',num2str(1-alpha),'\theta_{l} (K)'])
ylabel('q_t (g/kg)')

% title([fname,', h = ',num2str(floor(squeeze(mean(gps_alt(cond))))),' m'])
% if nargin>=7
%     if ~isempty(varargin{3})
%         title([varargin{3},', h = ',num2str(floor(squeeze(mean(gps_alt(cond))))),'m'])
%     end
% end

% colormap(cmap)
% hc=colorbar;
% ylabel(hc,'m/s')
% caxis([-V_max,V_max])

xlim([mean(Tmix(cond))-10,mean(Tmix(cond))+10])
if nargin>=5
    if ~isempty(varargin{1})
        xlim([varargin{1}])
    end
end

ylim([0,15])
if nargin>=6
    if ~isempty(varargin{2})
        ylim([varargin{2}])
    end
end

hold on
sec=squeeze(mean(hour(cond)))*3600;
plot_profile_tmixqt_06212020(fname_profile,p_leg,sec)