function y=equi_pot(T,p,r)
% Calculate equivalent potential temperature, following Wikipedia
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
syms x;
p(isnan(p))=0;
r(isnan(r))=0;
% INVes=matlabFunction(finverse(es(x)));
% Td=INVes(r/18*29.*p*100);
Td=arrayfun(@(rr)single(fzero(@(t)es(t)-rr,290)),r/18*29.*p*100);
%Td=zeros(size(T));
%for i=1:length(T(:))
%   Td(i)=fzero(@(x)es(x)-r(i)/18*29*p(i)*100,300);
%end
T_L=1./(1./(T-56)+log(T./Td)/800)+56;
theta_L=T.*(1000./p).^(287/1004).*...
    (T./T_L).^(0.28*r);
y=theta_L.*exp((3036./T_L-1.78).*r.*(1+0.448*r));