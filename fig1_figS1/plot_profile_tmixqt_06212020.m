function plot_profile_tmixqt_06212020(fname,p_leg,sec)
tt=160;
time=ncread(fname,'time');
tt=sum(time<sec);
tt=tt;
alpha=0.272;
tdry_sonde=ncread(fname,'temperature');
tdry_sonde=tdry_sonde(:,tt)-273.15;
pres_sonde=ncread(fname,'pressure');
pres_sonde=pres_sonde*10; % Convert from kPa to hPa
pres_sonde=pres_sonde(:,tt);
r_sonde=ncread(fname,'waterVaporMixingRatio')/1000;
r_sonde=r_sonde(:,tt);
r_sonde(r_sonde<0.1e-3)=nan;
equi1_sonde=equi_pot(tdry_sonde+273.15,pres_sonde,r_sonde);
plot(equi1_sonde*alpha+(1-alpha)*(tdry_sonde+273.15).*...
    (1000./pres_sonde).^(287/1004),r_sonde*1000,'r','linewidth',2)
hold on
% plot(equi1_sonde*alpha+(1-alpha)*(tdry_sonde+273.15).*...
%     (1000./pres_sonde).^(287/1004),r_sonde*1000,'ro')
equi_leg=interp1(pres_sonde,equi1_sonde*alpha+...
    (1-alpha)*(tdry_sonde+273.15).*(1000./pres_sonde).^(287/1004),p_leg);
r_leg=interp1(pres_sonde,r_sonde,p_leg);
% plot(equi_leg,r_leg*1000,'ko','MarkerSize',6)
