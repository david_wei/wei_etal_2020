function plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,varargin)
% Copied from ./plot_samples_tmixqt.m on 04/07/2020. Accepting n_start and
% n_end array
dist_limit=5;
if (nargin>=7)&&(~isempty(varargin{3}))
    dist_limit=varargin{3};
end
color_code=zeros(size(n_start));
if (nargin>=8)&&(~isempty(varargin{4}))
    color_code=varargin{4};
end
   
alp=0.272;
if (nargin>=9)&&(~isempty(varargin{5}))
    alp=varargin{5};
end

do_profile=false;
if (nargin>=10)&&(~isempty(varargin{6}))
    do_profile=varargin{6}{1};
    sec1=varargin{6}{2};
    sec2=varargin{6}{3};
end

ct=[];
if (nargin>=11)&&(~isempty(varargin{7}))
    ct=varargin{7};
end

fname2=[];
if (nargin>=12)&&(~isempty(varargin{8}))
    fname2=varargin{8};
end

fname3=[];
if (nargin>=13)&&(~isempty(varargin{9}))
    fname3=varargin{9};
end

fname4=[];
if (nargin>=14)&&(~isempty(varargin{10}))
    fname4=varargin{10};
end

if (nargin>=15)&&(~isempty(varargin{11}))
    fname_srf=varargin{11};
    time_srf=ncread(fname_srf,'time_offset');
    pres_02m=ncread(fname_srf,'pres_02m');
    pres_25m=ncread(fname_srf,'pres_25m');
    pres_60m=ncread(fname_srf,'pres_60m');
    temp_02m=ncread(fname_srf,'temp_02m')+273.15;
    temp_25m=ncread(fname_srf,'temp_25m')+273.15;
    temp_60m=ncread(fname_srf,'temp_60m')+273.15;
    r_02m=ncread(fname_srf,'mixing_ratio_02m')/1000;
    r_25m=ncread(fname_srf,'mixing_ratio_25m')/1000;
    r_60m=ncread(fname_srf,'mixing_ratio_60m')/1000;
    equi_02m=equi_pot(temp_02m,pres_02m,r_02m);
    equi_25m=equi_pot(temp_25m,pres_25m,r_25m);
    equi_60m=equi_pot(temp_60m,pres_60m,r_60m);
    tmix_02m=alp*equi_02m+(1-alp)*temp_02m.*(1000./pres_02m).^(287/1004);
    tmix_25m=alp*equi_25m+(1-alp)*temp_25m.*(1000./pres_25m).^(287/1004);
    tmix_60m=alp*equi_60m+(1-alp)*temp_60m.*(1000./pres_60m).^(287/1004);
end

xyrange_lr={}; % Only used when color_code(ii)==-1.5
ii_lr=0;
if (nargin>=16)&&(~isempty(varargin{12}))
    xyrange_lr=varargin{12};
end

syms x
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
INVes(x)=finverse(es(x));
% time=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','time');
% time_bounds=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','time_bounds');
% LH_baebbr_arm=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','LH_baebbr');
% LH_qcecor_arm=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','LH_qcecor');
% T_p=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','T_p');
% T_sfc=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','T_sfc');
% Td_p=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','Td_p');
% rh_sfc=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','rh_sfc');
% p=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','p');
% p_sfc=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','p_sfc');
% time_sfc=ncread('../sgparmbeatmC1.c1.20090101.000000.nc','time');
% Td_sfc=double(vpa(INVes(es(T_sfc).*rh_sfc/100),3));
hour=ncread(fname,'hour');
lat=ncread(fname,'lat');
lon=ncread(fname,'lon');
gps_alt=ncread(fname,'gps_alt');
pres=ncread(fname,'pres');
vert_wind=ncread(fname,'vert_wind');
vert_wind(vert_wind<-100)=nan;
aircraft_east_vel=ncread(fname,...
    'aircraft_east_vel');
aircraft_north_vel=ncread(fname,...
    'aircraft_north_vel');
lwc_gerber=ncread(fname,'lwc_gerber');
specific_humid=ncread(fname,'specific_humid');
wvmr_dlh=ncread(fname,'wvmr_dlh');
temperature=ncread(fname,'temperature');
equi_potential_temp=ncread(fname,...
    'equi_potential_temp');
rh=ncread(fname,'rh');
% rh=wvmr_dlh/1000./(es(temperature+273.15)./pres/100*18/29);
rho=(pres*100/287./(temperature+273.15));
cond=vert_wind>-100;
cond=lwc_gerber>-0.01;
dist=sqrt((lat-36.605).^2+(lon-97.485).^2)*110;
cond=cond&(dist<dist_limit);
rs=min(es(temperature+273.15)/100./pres*18/29,wvmr_dlh/1000);
rs(rs<0)=es(temperature(rs<0)+273.15)/100./pres(rs<0)*18/29.*rh(rs<0)/100;
% r_s=wvmr_dlh/1000;
% r_s=min(es(temperature+273.15)/100./pres*18/29,wvmr_dlh/1000.*(1+...
%     (randn(size(wvmr_dlh))-0.5)*0.5));
% rs(rs<0)=nan;
equi_potential_temp3=equi_pot(temperature+273.15,pres,rs);
% 04/07/2020
equi_potential_temp3=real(equi_potential_temp3);
% Highest triangle, run after CHECKlevel*.m

trange=zeros(size(hour));
trange(n_start:n_end)=1;

% 04/07/2020
cmap=lines;
cmap=cmap(2:end,:);
p_legend=[];
nn=0;
for ii=1:length(n_start)
    trange=zeros(size(hour));
    trange(n_start(ii):n_end(ii))=1;
    cond1=cond;
    cond=cond&trange;
%     find(cond,1,'first'),find(cond,1,'last')
V_max=10;
tt=3402;% time for Radiosonde profile
p_leg=squeeze(mean(pres(trange==1)));% for leg 1
p_leg=pres(trange==1);% for leg 1
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));

% plot(equi_pot(T_sfc(tt),p_sfc(tt),...
%     es(T_sfc(tt))/100/p_sfc(tt)*18/29*rh_sfc(tt)/100)*alpha+...
%     (1-alpha)*(T_sfc(tt)*(1000/p_sfc(tt))^(287/1004)),...
%     es(T_sfc(tt))/100/p_sfc(tt)*18/29*rh_sfc(tt)/100,'bo','MarkerSize',6)
% xlim([310,320])
% ylim([4e-3,7e-3])
hold on
axis ij

% tdry_sonde=ncread('sgpsondewnpnC1.b1.20090522.113000.nc','tdry');
% rh_sonde=ncread('sgpsondewnpnC1.b1.20090522.113000.nc','rh');
% pres_sonde=ncread('sgpsondewnpnC1.b1.20090522.113000.nc','pres');
% r_sonde=es(tdry_sonde+273.15)/100./pres_sonde*18/29.*rh_sonde/100;
% equi1_sonde=equi_pot(tdry_sonde+273.15,pres_sonde,r_sonde);
% plot(equi1_sonde*alpha+(1-alpha)*(tdry_sonde+273.15).*...
%     (1000./pres_sonde).^(287/1004),r_sonde,'r')

% equi_leg=interp1(pres_sonde,equi1_sonde*alpha+...
%     (1-alpha)*(tdry_sonde+273.15).*(1000./pres_sonde).^(287/1004),p_leg);
% r_leg=interp1(pres_sonde,r_sonde,p_leg);
% plot(equi_leg,r_leg,'ro','MarkerSize',6)

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
syms x;
INVes(x)=finverse(es(x));
% 04/07/2020
% plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
%     p_leg,0:0.1e-3:20e-3)*alpha+(1-alpha)*...
%     (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
%     (1000/p_leg)^(287/1004)),0:0.1e-3:20e-3,'b')

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
% cond=wvmr_dlh>0&lwc_gerber>0.1;
% cond=wvmr_dlh>0&lwc_gerber>-0.01;
% cond=cond&trange;
axis ij
hold on
tmp1=vert_wind(cond);
% bluewhitered
Tmix=equi_potential_temp3*alp+...
    (1-alp)*((temperature+273.15).*(1000./pres).^(287/1004)-...
    2.5e6/1004*lwc_gerber/1000./rho);
% 04/07/2020
if color_code(ii)==1
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,dist(cond),'filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
    caxis([0,max(dist(cond))])
    hcb=colorbar;
    hcb.Title.String='Dist to CF';
elseif color_code(ii)==1.1
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,dist(cond),'filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
    caxis([0,max(dist(cond))])
    hcb=colorbar;
    hcb.Title.String='Dist to CF';
    T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
end
elseif color_code(ii)==0
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,'k','filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
%     hs(ii).MarkerFaceColor=cmap(ii,:);
%     hs(ii).MarkerEdgeColor=cmap(ii,:);
hs(ii).MarkerFaceAlpha=0.1;
hs(ii).MarkerEdgeAlpha=0.1;
elseif color_code(ii)==-1
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,'k','filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
hs(ii).MarkerFaceAlpha=0.5;
hs(ii).MarkerEdgeAlpha=0.5;
elseif color_code(ii)==-1.1
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,'k','filled','DisplayName',['Sampled during ',num2str(floor([squeeze(min(hour(cond)))*3600,squeeze(max(hour(cond)))*3600])),' s UTC',', Within ',num2str(max(dist(cond))),' km away from CF']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
hs(ii).MarkerFaceAlpha=0.5;
hs(ii).MarkerEdgeAlpha=0.5;
elseif color_code(ii)==-2
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),3,'k','filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
hs(ii).MarkerFaceAlpha=0.2;
hs(ii).MarkerEdgeAlpha=0.2;

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
end
elseif color_code(ii)==-3
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),3,'k','filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
hs(ii).MarkerFaceAlpha=0.2;
hs(ii).MarkerEdgeAlpha=0.2;

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),0:0.1e-3:20e-3,'color',cmap(ii,:))
elseif color_code(ii)==-3.1
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,dist(cond),'filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
%     hs(ii).MarkerFaceColor=cmap(ii,:);
%     hs(ii).MarkerEdgeColor=cmap(ii,:);
% hs(ii).MarkerFaceAlpha=0.2;
% hs(ii).MarkerEdgeAlpha=0.2;
colorbar

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),0:0.1e-3:20e-3,'color',cmap(ii,:))
elseif color_code(ii)==-3.1
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,dist(cond),'filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF']);
%     hs(ii).MarkerFaceColor=cmap(ii,:);
%     hs(ii).MarkerEdgeColor=cmap(ii,:);
% hs(ii).MarkerFaceAlpha=0.2;
% hs(ii).MarkerEdgeAlpha=0.2;
colorbar

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),0:0.1e-3:20e-3,'color',cmap(ii,:),'DisplayName','Saturation line')
elseif color_code(ii)==-3.2
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,dist(cond),'filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF, pressure = ',num2str(mean(p_leg)),' hPa']);
%     hs(ii).MarkerFaceColor=cmap(ii,:);
%     hs(ii).MarkerEdgeColor=cmap(ii,:);
% hs(ii).MarkerFaceAlpha=0.2;
% hs(ii).MarkerEdgeAlpha=0.2;
colorbar

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),0:0.1e-3:20e-3,'color',cmap(ii,:),'DisplayName','Saturation line')
sec=[squeeze(min(hour(cond)))*3600,squeeze(max(hour(cond)))*3600];
cond_srf=(time_srf>=sec(1))&(time_srf<=sec(2));
secs=[hour(cond)';hour(find(cond)+1)']*3600;
cond_srf=any(((time_srf-secs(1,:))>=0)&((time_srf-secs(2,:))<0),2);
plot(tmix_25m(cond_srf),r_25m(cond_srf),'x','color',cmap(ii,:),'DisplayName','Surface')

elseif color_code(ii)==-1.2
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,dist(cond),'filled','DisplayName',['Within ',num2str(max(dist(cond))),' km away from CF, pressure = ',num2str(mean(p_leg)),' hPa']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
% hs(ii).MarkerFaceAlpha=0.2;
% hs(ii).MarkerEdgeAlpha=0.2;
colorbar

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),0:0.1e-3:20e-3,'color',cmap(ii,:),'DisplayName','Saturation line')
sec=[squeeze(min(hour(cond)))*3600,squeeze(max(hour(cond)))*3600];
cond_srf=(time_srf>=sec(1))&(time_srf<=sec(2));
secs=[hour(cond)';hour(find(cond)+1)']*3600;
cond_srf=any(((time_srf-secs(1,:))>=0)&((time_srf-secs(2,:))<0),2);
plot(tmix_25m(cond_srf),r_25m(cond_srf),'x','color',cmap(ii,:),'DisplayName','Surface','Linewidth',50)

elseif color_code(ii)==-1.3
    nn=nn+1;
    hs(ii)=scatter(Tmix(cond),...
        (rs(cond)+lwc_gerber(cond)/1000./rho(cond))*1000,5,dist(cond),'filled','DisplayName',['Level ',num2str(nn),': ',num2str(floor(mean(p_leg))),' hPa']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
hs(ii).MarkerFaceAlpha=1;
hs(ii).MarkerEdgeAlpha=1;

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3)*1000,'--','Color',cmap(ii,:),'HandleVisibility','off')
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),(0:0.1e-3:20e-3)*1000,'color',cmap(ii,:),'HandleVisibility','off')
sec=[squeeze(min(hour(cond)))*3600,squeeze(max(hour(cond)))*3600];
cond_srf=(time_srf>=sec(1))&(time_srf<=sec(2));
secs=[hour(cond)';hour(find(cond)+1)']*3600;
cond_srf=any(((time_srf-secs(1,:))>=0)&((time_srf-secs(2,:))<0),2);
hss=scatter(tmix_25m(cond_srf),r_25m(cond_srf)*1000,50,'dk','HandleVisibility','off');
hss.MarkerFaceColor=cmap(ii,:);
hss.MarkerEdgeColor=[0,0,0];
hs(ii).MarkerEdgeAlpha=0.05;

elseif color_code(ii)==-1.4 % Switched with -1.4
    nn=nn+1;
    hs(ii)=scatter(Tmix(cond),...
        (rs(cond)+lwc_gerber(cond)/1000./rho(cond))*1000,8,dist(cond),'filled','DisplayName',['Level ',num2str(nn),': ',num2str(floor(mean(p_leg))),' hPa']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
    hs(ii).MarkerFaceAlpha=1;
    hs(ii).MarkerEdgeAlpha=1;
    
    Ttmp=Tmix(cond);
    qttmp=rs(cond)+lwc_gerber(cond)/1000./rho(cond);
    ii_lr=ii_lr+1;
    xyrange=xyrange_lr{ii_lr};
    plot([xyrange(1),xyrange(1),xyrange(2),xyrange(2),xyrange(1)],...
        [xyrange(3),xyrange(4),xyrange(4),xyrange(3),xyrange(3)]*1000,'Color',cmap(ii,:),'HandleVisibility','off','Linewidth',2)
    condtmp=(Ttmp>=xyrange(1))&(Ttmp<=xyrange(2))&(qttmp>=xyrange(3))&(qttmp<=xyrange(4));
%     plot(polyval(polyfit(qttmp(condtmp),Ttmp(condtmp),1),[0,1]),[0,1],'--','Color',cmap(ii,:),'HandleVisibility','off','Linewidth',2)
    hold on
    
    mdl = fitlm(double(qttmp(condtmp)),double(Ttmp(condtmp)),'linear');
    x_lr=(0:1e-4:20e-3)';
    [y_lr,dy_lr]=predict(mdl,x_lr);
    plot(y_lr,x_lr*1000,'--','Color',cmap(ii,:),'HandleVisibility','off','Linewidth',2)
    patch([dy_lr(:,1);flipud(dy_lr(:,2))]',[x_lr;flipud(x_lr)]'*1000,1,'facecolor',cmap(ii,:),'edgecolor','none','facealpha',0.3,'HandleVisibility','Off');
    
    

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3)*1000,'--','Color',cmap(ii,:),'HandleVisibility','off')
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),(0:0.1e-3:20e-3)*1000,'color',cmap(ii,:),'HandleVisibility','off')
sec=[squeeze(min(hour(cond)))*3600,squeeze(max(hour(cond)))*3600];
cond_srf=(time_srf>=sec(1))&(time_srf<=sec(2));
secs=[hour(cond)';hour(find(cond)+1)']*3600-150;
cond_srf=any(((time_srf-secs(1,:))>=0)&((time_srf-secs(2,:))<0),2);
hss=scatter(tmix_25m(cond_srf),r_25m(cond_srf)*1000,50,'dk','HandleVisibility','off');
hss.MarkerFaceColor=cmap(ii,:);
hss.MarkerEdgeColor=[0,0,0];
hs(ii).MarkerEdgeAlpha=0.5;

elseif color_code(ii)==-1.5 % Buoyancy sorted points excluded
    nn=nn+1;
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),8,dist(cond),'filled','HandleVisibility','off');
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
    hs(ii).MarkerFaceAlpha=1;
    hs(ii).MarkerEdgeAlpha=1;
    
    Ttmp=Tmix(cond);
    qttmp=rs(cond)+lwc_gerber(cond)/1000./rho(cond);
    ii_lr=ii_lr+1;
    xyrange=xyrange_lr{ii_lr};
    plot([xyrange(1),xyrange(1),xyrange(2),xyrange(2),xyrange(1)],...
        [xyrange(3),xyrange(4),xyrange(4),xyrange(3),xyrange(3)],'Color',cmap(ii,:),'HandleVisibility','off','Linewidth',2)
    condtmp=(Ttmp>=xyrange(1))&(Ttmp<=xyrange(2))&(qttmp>=xyrange(3))&(qttmp<=xyrange(4));
%     plot(polyval(polyfit(qttmp(condtmp),Ttmp(condtmp),1),[0,1]),[0,1],'--','Color',cmap(ii,:),'HandleVisibility','off','Linewidth',2)
    hold on
        

T_leg=squeeze(mean(temperature(cond)+273.15));
r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
Tv_leg=T_leg*(1+0.61*r_leg);
p_leg=mean(p_leg);
for Tv_tmp=Tv_leg(1)
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
    max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
    es(T_iso)/100/p_leg*18/29));
plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
    es(T_iso)/100/p_leg*18/29)),...
    (0.1e-3:0.1e-3:20e-3),'--','Color',cmap(ii,:),'HandleVisibility','off')
end
plot(equi_pot(vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg)),...
    p_leg,0:0.1e-3:20e-3)*alp+(1-alp)*...
    (vpa(INVes((0:0.1e-3:20e-3)*29/18*100*p_leg))*...
    (1000/p_leg)^(287/1004)),0:0.1e-3:20e-3,'color',cmap(ii,:),'HandleVisibility','off')
sec=[squeeze(min(hour(cond)))*3600,squeeze(max(hour(cond)))*3600];
cond_srf=(time_srf>=sec(1))&(time_srf<=sec(2));
secs=[hour(cond)';hour(find(cond)+1)']*3600;
cond_srf=any(((time_srf-secs(1,:))>=0)&((time_srf-secs(2,:))<0),2);
hss=scatter(tmix_25m(cond_srf),r_25m(cond_srf),20,'dk','HandleVisibility','off');
hss.MarkerFaceColor=cmap(ii,:);
hss.MarkerEdgeColor=[0,0,0];
hs(ii).MarkerEdgeAlpha=0.05;

    xtmp=equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
        2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
        es(T_iso)/100/p_leg*18/29));
    ytmp=(0.1e-3:0.1e-3:20e-3);
    p0=polyfit(ytmp(1:2),xtmp(1:2),1);
    qttmp=qttmp(condtmp);
    Ttmp=Ttmp(condtmp);
    [gxy,~,~]=regression_twolines(double(qttmp),double(Ttmp),p0);
    hs(ii)=scatter(Ttmp(gxy==2),qttmp(gxy==2),8,'>','filled','DisplayName',['Level ',num2str(nn),': ',num2str(floor(mean(p_leg))),' hPa']);
    hs(ii).MarkerFaceColor=cmap(ii,:);
    hs(ii).MarkerEdgeColor=cmap(ii,:);
    hs(ii).MarkerFaceAlpha=1;
    hs(ii).MarkerEdgeAlpha=1;
    
    x_lr=(0:1e-4:20e-3)';
%     mdl1=fitlm(double(qttmp(gxy==1)),double(Ttmp(gxy==1)),'linear');
    mdl2=fitlm(double(qttmp(gxy==2)),double(Ttmp(gxy==2)),'linear');
%     [y_lr1,dy_lr1]=predict(mdl1,x_lr);
    [y_lr2,dy_lr2]=predict(mdl2,x_lr);
%     plot(y_lr1,x_lr,'--','Color',cmap(ii,:),'HandleVisibility','off','Linewidth',2)
%     patch([dy_lr1(:,1);flipud(dy_lr1(:,2))]',[x_lr;flipud(x_lr)]',1,'facecolor',cmap(ii,:),'edgecolor','none','facealpha',0.3,'HandleVisibility','Off');
    plot(y_lr2,x_lr,'--','Color',cmap(ii,:),'HandleVisibility','off','Linewidth',2)
    patch([dy_lr2(:,1);flipud(dy_lr2(:,2))]',[x_lr;flipud(x_lr)]',1,'facecolor',cmap(ii,:),'edgecolor','none','facealpha',0.3,'HandleVisibility','Off');


elseif color_code(ii)==2
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),10,gps_alt(cond)-318,'filled','HandleVisibility','Off');
    caxis([min(gps_alt-318),max(gps_alt-318)])
    hcb=colorbar;
    hcb.Title.String='Pressure (hPa)';
elseif color_code(ii)==3
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),20,gps_alt(cond)-318,'filled','HandleVisibility','Off');
    caxis([min(gps_alt-318),max(gps_alt-318)])
    hcb=colorbar;
    hcb.Title.String='Pressure (hPa)';
    hs(ii).Marker='^';
elseif color_code(ii)==4
    hs(ii)=scatter(Tmix(cond),...
        rs(cond)+lwc_gerber(cond)/1000./rho(cond),20,gps_alt(cond)-318,'filled','HandleVisibility','Off');
    caxis([min(gps_alt-318),max(gps_alt-318)])
    hcb=colorbar;
    hcb.Title.String='Pressure (hPa)';
    hs(ii).Marker='d';
end

% poly_r=rs(cond)+lwc_gerber(cond)/1000./rho(cond);
% poly_t=Tmix(cond);
% plot(polyval(polyfit(poly_r(poly_r>0.01),poly_t(poly_r>0.01),1),0:0.001:0.02),...
%     0:0.001:0.02,'color',cmap(ii,:),'DisplayName','LR (>10g/kg)')
% p_legend(end+1)=p_leg;

hold on

if do_profile
    sec=[squeeze(min(hour(cond)))*3600+sec1,squeeze(max(hour(cond)))*3600+sec2];
    htmp=plot_profile_tmixqt_vertical_05042020(fname_profile,p_leg,sec,alp,ct);
    if ~isempty(htmp)
    htmp.Color=cmap(ii,:);
    end
    if ~isempty(fname2)
        plot_sondeadjust_05052020(fname2,alp)
    end
    if ~isempty(fname3)
        plot_rlprofbe_05052020(fname3,sec,alp)
    end
    if ~isempty(fname4)
        plot_sondeadjust_05052020(fname4,alp)
    end
end

cond2=cond;
cond=cond1;
end
cond=cond2;

tmp=hist3([Tmix(cond),...
    rs(cond)+lwc_gerber(cond)/1000./rho(cond)],...
    'ctrs',{281:0.1:340,0:0.1e-3:20e-3})';
pertmp=arrayfun(@(x)sum(tmp(tmp>x))/sum(tmp(:)),tmp);
% [C,h]=contour(281:0.1:340,0:0.1e-3:20e-3,pertmp,1,'k');
% xlim([306.5,313])
axis ij
% clabel(C,h)


% T_leg=squeeze(mean(temperature(cond)+273.15));
% r_leg=squeeze(mean(rs(cond)+lwc_gerber(cond)/1000./rho(cond)));
% Tv_leg=T_leg*(1+0.61*r_leg);
% 
% for Tv_tmp=Tv_leg*[1]
% T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/p_leg*18/29)-...
%     max(0,q-es(T)/100/p_leg*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:20e-3);
% equi_iso=equi_pot(T_iso,p_leg,min(0.1e-3:0.1e-3:20e-3,...
%     es(T_iso)/100/p_leg*18/29));
% plot(equi_iso*alp+(1-alp)*(T_iso*(1000/p_leg)^(287/1004)-...
%     2.5e6/1004*max(0,(0.1e-3:0.1e-3:20e-3)-...
%     es(T_iso)/100/p_leg*18/29)),...
%     0.1e-3:0.1e-3:20e-3,'k--','DisplayName',['Density at ',num2str(p_leg),' hPa'])
% end

xlabel([num2str(alp),'\theta_{e}+',num2str(1-alp),'\theta_{l} (K)'])
ylabel('q_t (g/kg)')
title([fname,', h = ',num2str(floor(squeeze(mean(gps_alt(cond))))),'m'])

% colormap(cmap)
% hc=colorbar;
% ylabel(hc,'m/s')
% caxis([-V_max,V_max])

if (nargin>=5)&&(~isempty(varargin{1}))
    xlim([varargin{1}])
else
    xlim([mean(Tmix(cond))-10,mean(Tmix(cond))+10])
end

if (nargin>=6)&&(~isempty(varargin{2}))
    ylim([varargin{2}]*1000)
else
    ylim([0,20e-3])
end

hold on
sec=squeeze(mean(hour(cond)))*3600;
% plot_profile_tmixqt_vertical_04202020(fname_profile,p_leg,sec,alp)

legend('show')