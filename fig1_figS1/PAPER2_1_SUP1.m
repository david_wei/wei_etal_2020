figure('position',[100,100,600,900])
% sgtitle('12 cumulus cloud cases from RACORO','fontsize',20)
%% 05/22
fname='racoro.20090522.152715.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090522.001037.nc';
n_start=[734,4676,6726,8601,10710,23,3564,12540];
n_end=[3564,6540,8384,10450,12540,734,4676,14880];
tag=[-1.3,-1.3,-1.3,-1.3,-1.3,nan,nan,nan];
xyrange_lr={[308,311,6e-3,12e-3]};
subplot(3,2,2)
plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[307,313],[0.002,0.014],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090522.000000.cdf',xyrange_lr)
title('05/22/2009')
legend('fontsize',6,'location','southeast')
%% 05/24
fname='racoro.20090524.152824.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090524.001035.nc';
n_start=[387,1802,4993,7000,9084,11110,14590,3850,13020];
n_end=[1537,3850,6765,8903,10980,13020,14990,4993,14590];
tag=[-1.4,-1.3,-1.3,nan,-1.3,-1.4,nan,nan,nan];
xyrange_lr={[306.5,309,8e-3,11.8e-3],[307,310,4e-3,13e-3],[306.3,310.9,2.5e-3,12e-3]};
subplot(3,2,4)
plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[306,311],[0.002,0.014],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090524.000000.cdf',xyrange_lr)
title('05/24/2009')
legend('fontsize',6,'location','northeast')
%% 06/09
% fname='racoro.20090609.162912.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090609.001043.nc';
% n_start=[1694,5490,6086,9029,674,12400];
% n_end=[5264,5899,8849,12400,1694,13380];
% tag=[-1.3,-1.4,-1.4,nan,nan,nan];
% xyrange_lr={[316,316.9,3.5e-3,8e-3],[315.5,318.5,9e-3,15e-3],[316,320,11e-3,14e-3]};
% xyrange_lr={[315.5,318.5,9e-3,15e-3],[316,318,8e-3,14e-3]};
% subplot(3,2,4)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[315,320],[0.003,0.016],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090609.000000.cdf',xyrange_lr)
% title('06/09/2009')
% legend('fontsize',6,'location','northeast')
%% 06/11
% fname='racoro.20090611.170109.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090611.001034.nc';
% n_start=[839,2185,4290,6538,8515,9241,11220,2955,13350];
% n_end=[1936,2955,6303,8291,9004,10940,13350,4290,14860];
% tag=[nan,-1.3,-1.3,-1.3,-1.4,-1.4,-1.3,nan,nan];
% xyrange_lr={[309,311.5,8e-3,13e-3],[310.5,312,8e-3,13e-3]};
% subplot(3,2,5)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[308,313],[0.004,0.014],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090611.000000.cdf',xyrange_lr)
% title('06/11/2009')
% legend('fontsize',6,'location','northwest')
%% 03/30
% fname='racoro.20090330.192934.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090330.001043.nc';
% n_start=[659,5238,7307,10370,12570,3452];
% n_end=[3452,7083,10150,12180,13720,5238];
% tag=[-1.3,-1.4,nan,nan,nan,nan];
% xyrange_lr={[303,303.7,3.3e-3,6e-3]};
% subplot(3,2,1)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[301,305],[0.003,0.008],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090330.000000.cdf',xyrange_lr)
% title('03/30/2009')
% legend('fontsize',6,'location','northwest')
%% 05/06
% fname='racoro.20090506.162328.nc';
% fname_profile='sgpaeri01prof3feltzC1.c1.20090506.001036.nc';
% n_start=[608,1104,2045,3572,4337,5914,7711,9637,11850,14200,1,795,13250];
% n_end=[795,1752,3070,3854,4922,7326,9367,11520,13250,15330,608,1104,14200];
% tag=[nan,nan,nan,nan,-1.4,-1.3,-1.3,-1.3,nan,nan,nan,nan,nan];
% xyrange_lr={[302,306.5,6e-3,10e-3]};
% subplot(3,2,2)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[301,307],[0.006,0.013],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090506.000000.cdf',xyrange_lr)
% title('05/06/2009')
% legend('fontsize',6,'location','northwest')
%% 05/07
% fname='racoro.20090507.181222.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090507.001037.nc';
% n_start=[361,4749,6824,8837,10810,12790,15320,3302,14450];
% n_end=[3302,5269,8606,10490,12470,14450,16340,4749,15320];
% tag=[-1.3,-1.3,-1.3,nan,nan,-1.3,nan,nan,nan];
% subplot(4,4,4)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[310,316],[0.007,0.017],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090507.000000.cdf')
% title('05/07/2009')
% legend('fontsize',6,'location','northwest')
%% 06/08
% fname='racoro.20090608.161900.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090608.001034.nc';
% n_start=[2553,6137,7551,9827,12640,16100,4809,19120];
% n_end=[4809,7300,8683,12230,15670,19120,6137,20840];
% tag=[-1.3,nan,-1.3,-1.3,-1.3,-1.3,nan,nan];
% subplot(4,4,10)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[309,314],[0.002,0.014],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090608.000000.cdf')
% title('06/08/2009')
% legend('fontsize',6,'location','northwest')
%% 06/18
% fname='racoro.20090618.181748.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090618.001213.nc';
% n_start=[992,5699,6725,9434,13500,1,4031,14850];
% n_end=[4031,6438,8786,10220,14850,992,5699,16920];
% tag=[-1.3,-1.3,-1.3,nan,nan,nan,nan,nan];
% subplot(4,4,14)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[319,323],[0.000,0.016],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090618.000000.cdf')
% title('06/18/2009')
% legend('fontsize',6,'location','northeast')
%% 06/19
% fname='racoro.20090619.193212.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090619.001036.nc';
% n_start=[1086,2100,6290,10890,12030,13050,1,4663,13700];
% n_end=[1657,4663,8076,11740,12850,13700,1086,6290,15150];
% tag=[nan,-1.3,-1.3,nan,nan,nan,nan,nan,nan];
% subplot(4,4,15)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[318,323],[0.003,0.016],1000,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090619.000000.cdf')
% title('06/19/2009')
% legend('fontsize',6,'location','northwest')
%% 06/20
% fname='racoro.20090620.172733.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090620.001029.nc';
% n_start=[937,6200,9025,9802,1,4031,15520];
% n_end=[4031,7485,9448,10680,937,5544,17080];
% tag=[-1.3,-1.3,nan,nan,nan,nan,nan];
% subplot(4,4,16)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[316,320],[0.002,0.017],1000,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090620.000000.cdf')
% title('06/20/2009')
% legend('fontsize',6,'location','southwest')
%% 05/23
fname='racoro.20090523.142504.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090523.001030.nc';
n_start=[3666,4374,5162,5932,8061,10180,12350,1,1389,14180];
n_end=[4210,4957,5663,7813,9954,12080,14180,1389,3666,16280];
tag=[-1.3,nan,-1.3,-1.3,-1.3,-1.3,-1.4,nan,nan,nan];
xyrange_lr={[307.2,310.8,5.5e-3,13e-3]};
subplot(3,2,3)
plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[307,311],[0.005,0.015],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090523.000000.cdf',xyrange_lr)
title('05/23/2009')
legend('fontsize',6,'location','southwest')
%% 04/16
fname='racoro.20090416.170002.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090416.001111.nc';
n_start=[731,4259,6470,8384,10360,3190,12050];
n_end=[3190,6176,8150,10010,12050,4259,13410];
tag=[-1.3,-1.3,-1.3,-1.3,-1.3,nan,nan];
xyrange_lr={[298.1,300.5,2e-3,7.9e-3],[299.8,300.4,0.1e-3,5e-3]};
subplot(3,2,1)
plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[298,303],[0,0.008],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090416.000000.cdf',xyrange_lr)
title('04/16/2009')
legend('fontsize',6,'location','southeast')
%% 05/13
% fname='racoro.20090513.153238.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090513.001033.nc';
% n_start=[502,2215,9006,9952,10840,11420,12420,983,8353,9501,12970];
% n_end=[983,8353,9501,10500,11290,12220,12970,2215,9006,9952,13960];
% tag=[nan,-1.3,-1.3,nan,-1.3,-1.3,-1.3,nan,nan,nan,nan];
% subplot(3,2,3)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[312,317],[0.007,0.016],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090513.000000.cdf')
% title('05/13/2009')
% legend('fontsize',6,'location','northwest')
%% 05/26
fname='racoro.20090526.165720.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090526.001043.nc';
n_start=[364,4715,6795,8939,11050,14210,3660,12840];
n_end=[3660,6554,8671,10790,12840,14740,4715,14210];
tag=[-1.3,-1.3,-1.3,-1.3,-1.3,nan,nan,nan];
subplot(3,2,5)
plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[307,314],[0.005,0.013],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090526.000000.cdf')
title('05/26/2009')
legend('fontsize',6,'location','northeast')
%% 06/03
fname='racoro.20090603.185537.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090603.001041.nc';
n_start=[455,3758,5936,8186,10380,12560,2751,14560];
n_end=[2751,5705,7820,10100,12220,14560,3758,15850];
tag=[-1.3,-1.3,-1.3,-1.3,-1.3,-1.3,nan,nan];
subplot(3,2,6)
plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[301,309],[0.003,0.011],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090603.000000.cdf')
title('06/03/2009')
legend('fontsize',6,'location','northeast')
%% 06/12
% fname='racoro.20090612.180243.nc';
% fname_profile='sgpaeri01prof3feltzC1.s1.20090612.001056.nc';
% n_start=[1678,4033,6413,9092,10550,596,11030];
% n_end=[3795,6149,8536,10010,11030,1678,11750];
% tag=[-1.4,-1.4,-1.3,nan,-1.4,nan,nan];
% xyrange_lr={[314,315.5,9e-3,12e-3],[313.8,315.6,11e-3,13e-3],[313.5,315.8,8e-3,14e-3]};
% subplot(3,2,6)
% plot_samples_tmixqt_vertical_06232020(fname,fname_profile,n_start,n_end,[310,316],[0.008,0.015],10,tag,0.272,{false,0,0},[],[],[],[],'./surface_sgpc1/sgp1twrmrC1.c1.20090612.000000.cdf',xyrange_lr)
% title('06/12/2009')
% legend('fontsize',6,'location','northwest')
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_1_SUP1.png'])