CDF   0   
      time             Date      Fri Jun  5 05:38:59 GMT 2009       Version       "$State: process-vap-twrmr-1.5-1 $      IDL_Tools_Lib_Version         &$State: ds-idltools-idltools-1.18-0 $      IDL_Version       .VAP coded in IDL (sparc / sunos / unix / 5.4)      Command_Line      */apps/process/bin/twrmr -g 30 -d 20090603      Number_Input_Platforms        5      Input_Platforms       Vsgp30smosE13.b1, sgpthwapsC1.b1, sgp30twr10xC1.b1, sgp30twr25mC1.b1, sgp30twr60mC1.b1      Averaging_Int         30 min     Surface_selection_order       6SMOS used as primary surface obs, THWAPS as secondary      Tower_selection_order         ?SE elevator used as primary tower obs, W elevator as secondary     zeb_platform      sgp30twrmrC1.c1    missing-data      ******        ,   	base_time                string        3-Jun-2009,00:00:00 GMT    	long_name         Base time in Epoch     units         $seconds since 1970-1-1 0:00:00 0:00         �   time_offset                 	long_name         Time offset from base_time     units         %seconds since 2009-6-3 00:00:00 0:00            �   pres_02m                	long_name         Barometric pressure at 2 m     units         mb              pres_25m                	long_name         Barometric pressure at 25 m    units         mb             pres_60m                	long_name         Barometric pressure at 60 m    units         mb             temp_02m                	long_name         Temperature at 2 m     units         degC               temp_25m                	long_name         Temperature at 25 m    units         degC               temp_60m                	long_name         Temperature at 60 m    units         degC               temp_02m_SMOS                   	long_name         !Temperature at surface from SMOS       units         degC               temp_02m_THWAPS                 	long_name         #Temperature at surface from THWAPS     units         degC               temp_25m_SE                 	long_name         %Temperature at 25 m from SE elevator       units         degC                
temp_25m_W                  	long_name         $Temperature at 25 m from W elevator    units         degC            $   temp_60m_SE                 	long_name         %Temperature at 60 m from SE elevator       units         degC            (   
temp_60m_W                  	long_name         $Temperature at 60 m from W elevator    units         degC            ,   rh_02m                  	long_name         Relative humidity at 2 m       units         %           0   rh_25m                  	long_name         Relative humidity at 25 m      units         %           4   rh_60m                  	long_name         Relative humidity at 60 m      units         %           8   rh_02m_SMOS                 	long_name         'Relative humidity at surface from SMOS     units         %           <   rh_02m_THWAPS                   	long_name         )Relative humidity at surface from THWAPS       units         %           @   	rh_25m_SE                   	long_name         +Relative humidity at 25 m from SE elevator     units         %           D   rh_25m_W                	long_name         *Relative humidity at 25 m from W elevator      units         %           H   	rh_60m_SE                   	long_name         +Relative humidity at 60 m from SE elevator     units         %           L   rh_60m_W                	long_name         *Relative humidity at 60 m from W elevator      units         %           P   vap_pres_02m                	long_name         Vapor pressure at 2 m      units         mb          T   vap_pres_25m                	long_name         Vapor pressure at 25 m     units         mb          X   vap_pres_60m                	long_name         Vapor pressure at 60 m     units         mb          \   vap_pres_02m_SMOS                   	long_name         $Vapor pressure at surface from SMOS    units         mb          `   vap_pres_02m_THWAPS                 	long_name         &Vapor pressure at surface from THWAPS      units         mb          d   vap_pres_25m_SE                 	long_name         (Vapor pressure at 25 m from SE elevator    units         mb          h   vap_pres_25m_W                  	long_name         'Vapor pressure at 25 m from W elevator     units         mb          l   vap_pres_60m_SE                 	long_name         (Vapor pressure at 60 m from SE elevator    units         mb          p   vap_pres_60m_W                  	long_name         'Vapor pressure at 60 m from W elevator     units         mb          t   mixing_ratio_02m                	long_name          Water vapor mixing ratio at 2 m    units         g/kg            x   mixing_ratio_25m                	long_name         !Water vapor mixing ratio at 25 m       units         g/kg            |   mixing_ratio_60m                	long_name         !Water vapor mixing ratio at 60 m       units         g/kg            �   mixing_ratio_02m_SMOS                   	long_name         .Water vapor mixing ratio at surface from SMOS      units         g/kg            �   mixing_ratio_02m_THWAPS                 	long_name         0Water vapor mixing ratio at surface from THWAPS    units         g/kg            �   mixing_ratio_25m_SE                 	long_name         2Water vapor mixing ratio at 25 m from SE elevator      units         g/kg            �   mixing_ratio_25m_W                  	long_name         1Water vapor mixing ratio at 25 m from W elevator       units         g/kg            �   mixing_ratio_60m_SE                 	long_name         2Water vapor mixing ratio at 60 m from SE elevator      units         g/kg            �   mixing_ratio_60m_W                  	long_name         1Water vapor mixing ratio at 60 m from W elevator       units         g/kg            �   lat              	long_name         north latitude     units         degrees         �   lon              	long_name         east longitude     units         degrees         �   alt              	long_name         	altitude       units         meters above Mean Sea Level         �J%� Bk����RC�          Ds~�DrܼDq��A��A���A��A��A��
A���A�^5A��A���B��B
�B	DB��A��B
�A���B	DB�+AP  A^�DAXVAP  A[�A^�DAB��AXVA`��A�A�vA	WA�ALA�v@��\A	WA��@�      Ds��Dr�gDq�,A��A��A�=qA��A�Q�A��A藍A�=qA痍B33BN�B	1'B33B{BN�A�ƧB	1'B�AS�
A]AWt�AS�
Ag34A]AB~�AWt�A_�7A	�A �Aj7A	�A��A �@�%�Aj7A�@�      Ds��DsDDrtA�(�A��A�A�(�A���A��A�-A�A�S�B�B�B
e`B�B+33B�A�`BB
e`B��AU�A]�OAV�HAU�Ar=qA]�OAB�`AV�HA]t�A
j5A��A��A
j5A8A��@���A��ARx@�     Ds��Ds�Dr�A�(�A��A��
A�(�A��A��A�A��
A��B1�RB+B��B1�RB333B+B��B��B�=Ad  AbM�AY33Ad  Ao33AbM�AHM�AY33A`~�A�gA�Az�A�gA��A�A� Az�AM�@�      Ds��Ds/�Dr7<A�(�A�n�Aӥ�A�(�A�{A�n�Aҕ�Aӥ�A���B9�B3'�B)M�B9�BL�\B3'�BB)M�B,�Ag34As
>Ai&�Ag34A��As
>AX9XAi&�An�xA�A�A�A�A*��A�A5A�Aǥ@��     Dt*Dsf_Drj�A�{A��^A�JA�{A�33A��^A��A�JA�
=B�B�RoB�~�B�B���B�RoB��-B�~�B�A�G�A��jA���A�G�A�  A��jA�(�A���A�=qA?J�AO
�AMUA?J�AX"�AO
�A;��AMUAK:@�     Dt7
Ds��Dr�_A���A�G�A��RA���A�p�A�G�A�bNA��RA��B�  B���B���B�  B���B���B�w�B���B�[#A�Q�A�  A��
A�Q�A��A�  A��\A��
A��kAM��A]��A[�FAM��AT�A]��AMVA[�FAY @Ȝ     Dt_\Ds��Dr�A��RA���A�?}A��RA�  A���A�7LA�?}A�x�B�ffB�+B��PB�ffB���B�+B���B��PB� �A�A���A�ȴA�A��A���A���A�ȴA���AG�gAUO�AS��AG�gAJ&pAUO�AGRAS��AR`�@�      Dt�pDs�IDr�A�(�A���A�v�A�(�A�A���A��mA�v�A���B�  B�B�~wB�  B�  B�B���B�~wB�'�A��A�
=A��7A��A�  A�
=A�XA��7A���A<��AI�0AH�NA<��A?�_AI�0A;��AH�NAF8�@Ϥ     Dt�HDs��Dr�aA��A��#A���A��A���A��#A�A�A���A��B���B��LB�,B���B�ffB��LB�/�B�,B��A�p�A��\A�A�p�A���A��\A��A�A�/A7+�AE3AB��A7+�A>>�AE3A5�qAB��A@�@є     Dt��Ds�hDr�A�{A��7A�oA�{A�33A��7A���A�oA���B���B��5B�&fB���B���B��5B���B�&fB�{A��\A�A�JA��\A�(�A�A��tA�JA���A6 �ADU�AA8A6 �A=d!ADU�A53yAA8A?S%@�V     Dt��Ds�~Dr�A���A�C�A�&�A���A��A�C�A���A�&�A��B���B���B�ĜB���B�33B���B�RoB�ĜB���A�
>A�bA�XA�
>A���A�bA��A�XA��A6��ADq�AA��A6��A>EADq�A5�AA��A?�d@�     Dt��Ds�IDr�yA�\)A�l�A��wA�\)A�  A�l�A�^5A��wA��^B�33B��{B�  B�33B���B��{B��+B�  B�ŢA��RA��yA��kA��RA���A��yA���A��kA��!A8�;AE��AC}A8�;AA�AE��A7�AC}AB�@��     Dt�\Ds��Dr��A��A�~�A��\A��A��HA�~�A��9A��\A���B�33B�1B�^�B�33B�  B�1B�ڠB�^�B�C�A��A��A�~�A��A���A��A��#A�~�A�� A:$AF�AEŽA:$AA�AF�A9�$AEŽAC]�@؜     Dt��Ds�Dr��A���A��A�K�A���A�\)A��A�E�A�K�A�-B���B�5B�
B���B���B�5B��XB�
B��uA�=pA���A�?}A�=pA�z�A���A��`A�?}A��HA:�IAG��AF�dA:�IAC�AG��A:�kAF�dAC�V@�^     Dt]Ds֣Dr֑A��A��A�%A��A�
=A��A�ȴA�%A�hsB���B���B�~�B���B���B���B���B�~�B�kA�
=A��+A��-A�
=A���A��+A�n�A��-A��A;��AI�AGy�A;��AC�<AI�A;�AGy�AEX@�      Dt}pDsԵDrԠA��A��
A��RA��A�G�A��
A�;dA��RA�B�33B�XB�+B�33B���B�XB�:�B�+B�[�A�p�A�ƨA�ĜA�p�A�
>A�ƨA�v�A�ĜA�`BA<�[AIhtAG��A<�[AC�AIhtA=kAG��AE�,@��     Dtz�Ds�FDr�JA�
=A�A��
A�
=A���A�A�K�A��
A��B�33B��B�W
B�33B���B��B���B�W
B�AA�
=A�-A��yA�
=A�z�A�-A�~�A��yA�v�A;�!AI�vAFq�A;�!AC-�AI�vA;�,AFq�AE�@ߤ     Dt��Ds�bDr�aA�=qA��mA�1A�=qA��A��mA��-A�1A���B�ffB�T{B���B�ffB���B�T{B�+�B���B�e`A�
=A�ZA�{A�
=A�z�A�ZA���A�{A���A;�AKh�AF� A;�AE�AKh�A< �AF� AF�@�     Dt��Dt*Ds8A�33A�t�A���A�33A��RA�t�A���A���A�ȴB�33B�B���B�33B���B�B��B���B�uA���A��A��PA���A�\)A��A��A��PA�33A>!�AMYAIļA>!�AF�AMYA>E�AIļAG��@�     Dt�qDt�DsYA���A��A��-A���A�z�A��A���A��-A�"�B�  B��B�s3B�  B�  B��B�=�B�s3B��NA�A�ZA�
>A�A��A�ZA���A�
>A��FA:�AGOJAE�A:�A<k%AGOJA9TUAE�ACM�@�u     Dt� Dt&�Ds%�A���A���A���A���A�A���A�{A���A�x�B���B�}qB�H1B���B�33B�}qB���B�H1B��A���A���A�"�A���A�\)A���A�bA�"�A�\)A4�NAA
�A>~�A4�NA<&�AA
�A3�A>~�A:��@�V     Dt��Dt3Ds1�A�p�A��^A�^5A�p�A�z�A��^A�bA�^5A��B���B��B�t�B���B�  B��B�\B�t�B�A��A��PA�~�A��A��A��PA��A�~�A�z�A4j�A>5VA<G�A4j�A7�A>5VA2|�A<G�A9��@�7     Dt�GDtGbDsE�A���A���A��PA���A�ffA���A��;A��PA�S�B�33B���B�*B�33B���B���B���B�*B��\A�fgA��A�p�A�fgA�A��A�VA�p�A��DA0@�A:V�A8)�A0@�A4�;A:V�A/V/A8)�A6��@�     DugDt\mDsZ�A�G�A�bNA���A�G�A�{A�bNA�XA���A��^B�ffB�k�B��B�ffB�33B�k�B��B��B�@ A���A�33A�$�A���A��A�33A��^A�$�A��A-��A8T�A6bA-��A4�oA8T�A-(iA6bA4��@��     Du�Dth�Dsf�A�
=A�7LA�A�A�
=A�ffA�7LA�+A�A�A�Q�B���B�&�B�5�B���B���B�&�B��7B�5�B�lA��
A�JA�(�A��
A�(�A�JA��yA�(�A�K�A,�&A6�5A3�A,�&A2x�A6�5A*��A3�A2�4@��     Du${DtzeDsx5A�z�A�n�A�ĜA�z�A�
=A�n�A���A�ĜA��;B�ffB�kB�D�B�ffB���B�kB��B�D�B�wLA�ffA���A�/A�ffA�34A���A���A�/A�\)A*�VA5N�A1>A*�VA.��A5N�A'�6A1>A/�&@�     Du3�Dt��Ds�FA\)A���A��A\)A�ffA���A���A��A��B�ffB��B���B�ffB�  B��B��B���B��A�{A�33A��TA�{A�
=A�33AG�A��TA�9XA'�QA1�nA-��A'�QA+�wA1�nA%�A-��A-@�     Du:>Dt�Ds��A�(�A��FA��PA�(�A��
A��FA��A��PA��yB�33B��B�vFB�33B�  B��B��B�vFB���A�=qA�jA���A�=qA�
=A�jA|ffA���A�ĜA%VSA0�IA,��A%VSA+��A0�IA#A,��A+&�@�}     DuAGDt�Ds��A�ffA���A�oA�ffA�p�A���A�5?A�oA�33B�ffB�B�lB�ffB�33B�B�ݲB�lB�8�A|��A�Q�A���A|��A��A�Q�AvVA���A�%A#'A+#�A)��A#'A'4�A+#�A/A)��A'�6@�^     DuMqDt�[Ds��A��\A�^5A���A��\A�Q�A�^5A�A���A�|�B�33B�^5B�%`B�33B���B�^5B�6FB�%`B���A{
>A��A��yA{
>A��A��Au�#A��yA��!A!�A-qNA(��A!�A'|�A-qNA�iA(��A'@�?     DuT|Dt��Ds�xA��A�7LA��A��A�\)A�7LA�=qA��A�ffB�ffB�ŢB��`B�ffB���B�ŢB�|jB��`B���A{\)A�  A�l�A{\)A�ffA�  Au��A�l�A���A!�A+� A)MjA!�A(�A+� A�.A)MjA'f�@�      DuUDt�fDs��A��
A���A�n�A��
A�  A���A�l�A�n�A��B�33B�{B�^5B�33B���B�{B��B�^5B�A}G�A�hsA��A}G�A�=qA�hsAw��A��A�bNA#+eA,��A*2^A#+eA*�*A,��A��A*2^A)?@�     DuE�Dt�NDs�A�
=A���A�G�A�
=A�Q�A���A�jA�G�A��+B���B���B�A�B���B�33B���B�$�B�A�B���A~=pA���A�r�A~=pA�A���Ay��A�r�A���A#��A.fMA,LA#��A,��A.fMA!>A,LA*�X@��     DuJ=Dt�1Ds��A���A�;dA�"�A���A�(�A�;dA���A�"�A��mB�  B���B�YB�  B�  B���B���B�YB��A34A�%A�1A34A���A�%AxA�1A��wA$t�A.��A,��A$t�A(�A.��A 0�A,��A+F@��     DuE�Dt�lDs��A�
=A�r�A�G�A�
=A�Q�A�r�A�VA�G�A���B�\B��=B�JB�\B�  B��=B���B�JB�{A|Q�A�A�JA|Q�A��A�At1A�JA��TA"��A-Z�A*'�A"��A&��A-Z�A��A*'�A)��@�     Du>�Dt��Ds�FA�G�A�r�A��A�G�A���A�r�A�+A��A�l�B��B��B��B��B~
>B��B}��B��B�(sAv=qA�A��7Av=qA~=pA�AmdZA��7A�A�A��A)jgA&�wA��A#ۇA)jgA?�A&�wA&|�@�B�    Du1�Dt�>Ds��A��A�bNA���A��A�z�A�bNA�~�A���A��7Bz��B���B���Bz��Bl�IB���Bo�7B���B�#TAs�A�1'Ap�As�A{\)A�1'AkdZAp�A��A�A(`XA%��A�A"7A(`XA��A%��A%��@�     Du/\Dt��Ds��A�{A��A��7A�{A���A��A��HA��7A��Bs\)B�#�B���Bs\)BnG�B�#�Bf�CB���B���Ar{A�<A{�Ar{A{34A�<Af��A{�A|�RA�A%j�A#V'A�A!�A%j�A�"A#V'A$�@�#�    Du-pDt�$Ds�A�=qA��A��A�=qA���A��A��7A��A�1Bk=qB|9XB}�>Bk=qBY�
B|9XB^�RB}�>B}KAm�A~Av�RAm�Ar{A~Ac�Av�RAy�FA8bA$3�A �A8bA��A$3�AόA �A"
�@�     Du'
Dt�UDs� A��A�A���A��AÙ�A�A�M�A���A��BJ��B_�JB`�RBJ��B6B_�JB@�FB`�RB_��A^ffAjj�Ad1'A^ffA`  Ajj�AO/Ad1'Af�GA�AZmA��A�A$AZmA��A��A��@��    Du�Dtr�DsvdA��HA�^5A�5?A��HAȣ�A�^5A�5?A�5?A�oB/�\BB�sBE-B/�\B(G�BB�sB&uBE-BE�!AL��AX9XARQ�AL��AV=pAX9XA<=pARQ�AU�lA�ZAy)A+{A�ZA	�	Ay)@�g�A+{A
��@�u     Du�Dte�Dsi�A���A��#A��
A���A�p�A��#A�1A��
A�S�B�HB(��B),B�HB��B(��B|�B),B+p�A6{A@M�A9�PA6{A5p�A@M�A)�vA9�PA?��@��Y@���@��A@��Y@���@���@�_D@��A@�ޕ@��    Dt�DtJ�DsOEA�Q�A��
A�S�A�Q�A��A��
A�z�A�S�A��B�\BffB�/B�\A��RBffA�`BB�/B�qA�\A)S�A!��A�\A"ffA)S�A�vA!��A(�@�ak@���@�B@�ak@�[�@���@�Y�@�B@ٿ�@�V     Dt�Dt?oDsC�A��RA��+A��A��RA���A��+A��DA��A��;B (�B�?BF�B (�BG�B�?A���BF�BhsAA&��A�?AA0(�A&��A�A�?A&�R@�3@���@�au@�3@�A@���@��@�au@�s@�ƀ    Dt�DtE�DsI�A���A��A���A���A�\)A��A��A���A�=qB��B��Bn�B��B�B��B-Bn�BɺA&�RA2��A+�lA&�RA/�A2��A�A+�lA2�x@��2@�|@�3j@��2@�fM@�|@�B�@�3j@�]T@�7     Dt�Dt?@DsC5A��RA�5?A��/A��RAř�A�5?A��hA��/A�oB�B�RBz�B�B(�B�RB�/Bz�B�`A
>A+%A#��A
>A.fgA+%A%A#��A*�\@�
Y@�,�@��@�
Y@���@�,�@�,�@��@�w�@���    Dt�>Dt47Ds7�A�G�A�t�A���A�G�A�  A�t�A���A���A��hB
=BN�B�NB
=BG�BN�A�G�B�NB{�AA!�8A�AA"�RA!�8A��A�A n�@��@��c@���@��@��@��c@�%�@���@�GN