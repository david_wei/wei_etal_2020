CDF   0   
      time             Date      Sat Jun  6 05:43:32 GMT 2009       Version       "$State: process-vap-twrmr-1.5-1 $      IDL_Tools_Lib_Version         &$State: ds-idltools-idltools-1.18-0 $      IDL_Version       .VAP coded in IDL (sparc / sunos / unix / 5.4)      Command_Line      */apps/process/bin/twrmr -g 30 -d 20090605      Number_Input_Platforms        5      Input_Platforms       Vsgp30smosE13.b1, sgpthwapsC1.b1, sgp30twr10xC1.b1, sgp30twr25mC1.b1, sgp30twr60mC1.b1      Averaging_Int         30 min     Surface_selection_order       6SMOS used as primary surface obs, THWAPS as secondary      Tower_selection_order         ?SE elevator used as primary tower obs, W elevator as secondary     zeb_platform      sgp30twrmrC1.c1    missing-data      ******        ,   	base_time                string        5-Jun-2009,00:00:00 GMT    	long_name         Base time in Epoch     units         $seconds since 1970-1-1 0:00:00 0:00         �   time_offset                 	long_name         Time offset from base_time     units         %seconds since 2009-6-5 00:00:00 0:00            �   pres_02m                	long_name         Barometric pressure at 2 m     units         mb              pres_25m                	long_name         Barometric pressure at 25 m    units         mb             pres_60m                	long_name         Barometric pressure at 60 m    units         mb             temp_02m                	long_name         Temperature at 2 m     units         degC               temp_25m                	long_name         Temperature at 25 m    units         degC               temp_60m                	long_name         Temperature at 60 m    units         degC               temp_02m_SMOS                   	long_name         !Temperature at surface from SMOS       units         degC               temp_02m_THWAPS                 	long_name         #Temperature at surface from THWAPS     units         degC               temp_25m_SE                 	long_name         %Temperature at 25 m from SE elevator       units         degC                
temp_25m_W                  	long_name         $Temperature at 25 m from W elevator    units         degC            $   temp_60m_SE                 	long_name         %Temperature at 60 m from SE elevator       units         degC            (   
temp_60m_W                  	long_name         $Temperature at 60 m from W elevator    units         degC            ,   rh_02m                  	long_name         Relative humidity at 2 m       units         %           0   rh_25m                  	long_name         Relative humidity at 25 m      units         %           4   rh_60m                  	long_name         Relative humidity at 60 m      units         %           8   rh_02m_SMOS                 	long_name         'Relative humidity at surface from SMOS     units         %           <   rh_02m_THWAPS                   	long_name         )Relative humidity at surface from THWAPS       units         %           @   	rh_25m_SE                   	long_name         +Relative humidity at 25 m from SE elevator     units         %           D   rh_25m_W                	long_name         *Relative humidity at 25 m from W elevator      units         %           H   	rh_60m_SE                   	long_name         +Relative humidity at 60 m from SE elevator     units         %           L   rh_60m_W                	long_name         *Relative humidity at 60 m from W elevator      units         %           P   vap_pres_02m                	long_name         Vapor pressure at 2 m      units         mb          T   vap_pres_25m                	long_name         Vapor pressure at 25 m     units         mb          X   vap_pres_60m                	long_name         Vapor pressure at 60 m     units         mb          \   vap_pres_02m_SMOS                   	long_name         $Vapor pressure at surface from SMOS    units         mb          `   vap_pres_02m_THWAPS                 	long_name         &Vapor pressure at surface from THWAPS      units         mb          d   vap_pres_25m_SE                 	long_name         (Vapor pressure at 25 m from SE elevator    units         mb          h   vap_pres_25m_W                  	long_name         'Vapor pressure at 25 m from W elevator     units         mb          l   vap_pres_60m_SE                 	long_name         (Vapor pressure at 60 m from SE elevator    units         mb          p   vap_pres_60m_W                  	long_name         'Vapor pressure at 60 m from W elevator     units         mb          t   mixing_ratio_02m                	long_name          Water vapor mixing ratio at 2 m    units         g/kg            x   mixing_ratio_25m                	long_name         !Water vapor mixing ratio at 25 m       units         g/kg            |   mixing_ratio_60m                	long_name         !Water vapor mixing ratio at 60 m       units         g/kg            �   mixing_ratio_02m_SMOS                   	long_name         .Water vapor mixing ratio at surface from SMOS      units         g/kg            �   mixing_ratio_02m_THWAPS                 	long_name         0Water vapor mixing ratio at surface from THWAPS    units         g/kg            �   mixing_ratio_25m_SE                 	long_name         2Water vapor mixing ratio at 25 m from SE elevator      units         g/kg            �   mixing_ratio_25m_W                  	long_name         1Water vapor mixing ratio at 25 m from W elevator       units         g/kg            �   mixing_ratio_60m_SE                 	long_name         2Water vapor mixing ratio at 60 m from SE elevator      units         g/kg            �   mixing_ratio_60m_W                  	long_name         1Water vapor mixing ratio at 60 m from W elevator       units         g/kg            �   lat              	long_name         north latitude     units         degrees         �   lon              	long_name         east longitude     units         degrees         �   alt              	long_name         	altitude       units         meters above Mean Sea Level         �J(` Bk����RC�          Dt��Ds�?Dr�A��A�A��A��A���A�A�v�A��Aʏ\BffBl�B�#BffB�HBl�B�B�#B|�A1�AA�FA9�A1�A>�RAA�FA*VA9�AA%@��h@�&M@��a@��h@��Z@�&M@ۡF@��a@�Cy@�      Dt�
Ds�ZDr�HA��HA�{AżjA��HA��
A�{A��AżjA�;dB=qB}�B�bB=qB\)B}�B��B�bB��A8  ADA<ȵA8  AE��ADA.�CA<ȵADr�@�͇@�#=@��@�͇@���@�#=@�F@��@��i@�      Dt�>Ds�=Dr��A�ffAƣ�A�1'A�ffA�  Aƣ�Aȗ�A�1'A�33B�B��BVB�B-G�B��B	<jBVB�XA?\)AC&�A<r�A?\)AQAC&�A.��A<r�AC�T@�a$@���@�9�@�a$A2 @���@�*�@�9�@���@�     Dt��Ds��Dr�uA�A�(�A���A�A���A�(�A�1A���A�(�B?�
B4VB,{B?�
BS�QB4VBǮB,{B.��APQ�AUC�AK�APQ�Aa�AUC�A<Q�AK�AP��AB�A	�zA��AB�A?	A	�z@�MA��AZK@�      Dt�DsނDr�A��A�%A�-A��A�\)A�%A��A�-A�C�B\�BF�B3q�B\�Bn BF�B-N�B3q�B8+ATQ�A\��AL�xATQ�A`��A\��AA�TAL�xARI�A�>A�2A�=A�>A
�A�2@�^�A�=Az_@��     Dt�)Ds��Dr�A���A���A���A���A�=qA���A�~�A���A�v�Bo{BK��B1q�Bo{Bs��BK��B3��B1q�B6��AS
>AZQ�AH��AS
>A]�AZQ�A@1AH��ANbA"A+DA:$A"A�=A+D@��
A:$A��@�     Dt� Ds�6Dr�~A���A�I�A��!A���A�{A�I�A��mA��!A��-B�#�BW��B=�^B�#�B��=BW��B>�B=�^BBDAS\)A[��AN-AS\)A^=qA[��ACl�AN-AQG�A:|A@�A�PA:|AW�A@�@�V�A�PA��@Ȝ     Dt�4Ds�Dr��A�Q�A�p�A��TA�Q�A�Q�A�p�A��RA��TA���B�B`��BFm�B�B��B`��BG��BFm�BJ�`AS
>A^=qAS"�AS
>A\��A^=qAFZAS"�AV$�A�$A�QA��A�$A[
A�QA �A��A
�m@�      Dt��Dt0Ds�A�=qA�ȴA�&�A�=qA�p�A�ȴA�=qA�&�A��B�ffBh��BI�B�ffB�aHBh��BO�BI�BMQ�AS
>Aa��AS&�AS
>A]�Aa��AJI�AS&�AU�TA�AنA�]A�A<AنA��A�]A
��@Ϥ     DtǮDt�Ds�A�{A�I�A�v�A�{A���A�I�A�;dA�v�A���B�  Bn�BN�B�  B�aHBn�BU�#BN�BR�LAT(�Aat�AS7KAT(�A^{Aat�AI��AS7KATbNA��A��A�uA��A�A��A8.A�uA	�@є     Dt�pDt$Ds$VAqp�A�5?A��9Aqp�A��A�5?A���A��9A�ĜB���Bz��B]v�B���B���Bz��Ba�
B]v�B`��AT(�AaAZAT(�A\��AaAL1AZAZ1A��A�Ai}A��A\�A�A�AAi}Al/@�V     Dt�3Dt�Ds�Ah��A��A��hAh��A~�HA��A��jA��hA�B���B���B\�B���B���B���BhD�B\�B`�AR�\Aa�^AW34AR�\A\��Aa�^AM7LAW34AX{A��A�A��A��AH*A�Ax;A��A)�@�     Dt� Dt1Ds�Ab�\A���A��Ab�\AvffA���A�E�A��A�5?B���B�#�Be�B���B���B�#�Bo}�Be�Bi#�AQAbVA[�AQAZ�HAbVAM�A[�A[�AAO�A)GAA�AO�A�A)GA�'@��     Dt�Dt?Ds	A`��A�oA�z�A`��Az�RA�oA��7A�z�A�oB�  B�bNBg��B�  B�  B�bNBu�?Bg��Bj� AR=qAd�A\�yAR=qA]�Ad�APn�A\�yA\�AjVA�ZA`nAjVA�A�ZA��A`nAc!@؜     Dt�DtDs�A_
=A��A�{A_
=As�A��A���A�{A�VB�33B��'Bp��B�33B���B��'B�r-Bp��Bs�ARfgAhA�A_��ARfgA\��AhA�AS��A_��A^ȴA�A=�A2 A�A9�A=�A�?A2 A�p@�^     Dt��Dt�DsTA`z�A�S�A���A`z�A}�A�S�A���A���A���B���B��Bn5>B���B���B��B��Bn5>Bp�AV�\AiA\��AV�\Ac33AiAU�A\��A\  A
@8A��A.�A
@8A��A��A	��A.�Aʺ@�      Dt��Ds�Dr�Ao�A�JA�^5Ao�Aw\)A�JA���A�^5A���B�33B�\Bo�B�33B�  B�\B�/Bo�BrA\��AhE�A[�A\��Ad��AhE�ATZA[�A[
=AusAFwA�AusA��AFwA	5�A�A/@��     Dt��Ds��Dr�Ai�A�9XA�;dAi�Ao�
A�9XA�Q�A�;dA��hB���B�Bx��B���B�ffB�B�I�Bx��Bz�AY�AgC�A^=qAY�A`  AgC�AW
=A^=qA]hsA�A� ATjA�A|A� A�ATjA�@ߤ     Dt�4Ds��Dr�FAdz�A���A�%Adz�Aj{A���A���A�%A�K�B�  B��B��B�  B�  B��B�{�B��B���AX  Ae�A`ȴAX  A[�
Ae�AV1A`ȴA_�A7�A�"A��A7�A�VA�"A
PA��A �@�     Dt��Ds��Dr�9Ak�A�Q�A���Ak�Al��A�Q�A�A�A���A��9B���B���B�XB���B�ffB���B���B�XB�ZAX(�Ad��A^�AX(�A^�RAd��AShsA^�A]x�AQA��A�RAQA��A��A�jA�RA�P@�     Dt��Ds��Dr�AfffA��A�VAfffAu�A��A��yA�VA��B�  B�w�B��3B�  B���B�w�B�d�B��3B�V�AU��Ad�RA_��AU��A\��Ad�RARA_��A]��A	��A�A4dA	��Av�A�A�7A4dA1@�u     Dt��Ds��Dr��Ag33A�bNA�ƨAg33Af�HA�bNA��A�ƨA��\B�ffB�JB�mB�ffB�ffB�JB��3B�mB���ATQ�AchsA_dZATQ�AZ�HAchsAQ��A_dZA]�
A��AA�A��AGAA��A�A�@�V     Dt��Ds�Dr�AZ�\A��#A��AZ�\Ahz�A��#A�p�A��A��;B�ffB�8RB���B�ffB�33B�8RB�ۦB���B���ARfgAe�TA`5?ARfgA[�
Ae�TAQA`5?A]�
A��A��A�EA��A�[A��A��A�EA�@�7     Dt�*Dt�Ds �Ac�A���A��Ac�A_�A���A�1'A��A�bB�33B��B��HB�33B�  B��B��bB��HB�,�AS�
Ad��A`�CAS�
AV=pAd��AR��A`�CA^ĜAz�A��A�Az�A
)A��A
&A�A��@�     Dt��Dt$DsUAW�
A��A�x�AW�
Ag
=A��A���A�x�A�C�B�  B�,�B��B�  B�33B�,�B��B��B���AP��Aep�AaXAP��A[�
Aep�ARbNAaXA`�A��A]iAJ;A��A�.A]iA��AJ;Az}@��     DtҐDt(>Ds&�Ad��A�/A�JAd��A�Q�A�/A�&�A�JA�%B�33B��B�I�B�33B�  B��B��B�I�B��JA[
=Af �A`�0A[
=AmAf �ASoA`�0A_�TA�A�zA��A�AWOA�zAD�A��AEZ@��     Dt�Dt+�Ds+kA�Q�A��A� �A�Q�A���A��A�n�A� �A���B�ffB���B�gmB�ffB��)B���B�cTB�gmB��HAf�GAn�AcS�Af�GAp��An�AZ�jAcS�Aa\(A��A�5A�PA��ASkA�5AG�A�PA:�@�     Dt׮Dt/�Ds0qA��RA��A�
=A��RA���A��A��-A�
=A���B|G�B���B�B|G�Bdz�B���Bi"�B�B�gmAb=qAk7LAdQ�Ab=qAl  Ak7LATZAdQ�Adr�A�YA�A*vA�YA-A�A	<A*vA@@�     Dt��Dt.fDs0iA�
=A�z�A���A�
=A�
=A�z�A���A���A���Bgp�Bu�aBr��Bgp�BS�Bu�aBX�yBr��Bt�AdQ�Am��Ag��AdQ�Ao�Am��AUO�Ag��Ah��A'�A��AP�A'�A|[A��A	��AP�A	@�}     Dt��Dt$YDs'�A���A�|�A�ffA���AÅA�|�A��\A�ffA�bNBM�
B[��BZBM�
B9��B[��B?��BZB\A]�Ag"�Aa�
A]�Ac�Ag"�AL�`Aa�
Ac�<A�Am4A��A�A�eAm4A<�A��A�e@�^     Dt˅Dt%�Ds)�A�p�A�VA��A�p�A˙�A�VA��mA��A��mB?33BLQ�BK��B?33B/=qBLQ�B1hsBK��BM�{A\z�AdZA]�TA\z�Ad  AdZAH��A]�TAa�iA�A��A�A�A�bA��AqA�A^�@�?     Dt�qDtDs�A��A�x�A�n�A��A�ffA�x�A���A�n�A�~�B3G�B@T�B@�XB3G�B)=qB@T�B&=qB@�XBB�yAX��Ab�A\�AX��Ad(�Ab�AF{A\�A`ȴA�#A��ArA�#A�A��@��kArA�k@�      Dt� Dt
�DsSA���A¾wA�\)A���A�G�A¾wA�$�A�\)A�  B)z�B7B7��B)z�B!G�B7B��B7��B9�AW�
A^�HAX��AW�
Aap�A^�HAC?|AX��A]�wA�A>A�!A�A\YA>@���A�!A�}@�     Dt�{Ds��Ds�A�33A�~�A�?}A�33A�A�~�A�XA�?}A�A�B#��B0ɺB2�B#��B�B0ɺB�wB2�B4$�AZ�]A_�AY�FAZ�]Aep�A_�AD�:AY�FA_�hA�YA;�AHBA�YAA;�@��wAHBA#K@��     Dt��Ds�jDr��AҸRA���A�?}AҸRA���A���AΝ�A�?}A�n�B�B)P�B*�LB�B�B)P�B�B*�LB-  AW�A^5@AX��AW�Ahz�A^5@AD��AX��A_+A
�A��A�`A
�AA��@�"sA�`A�@��     Dt�
Ds�!Dr�#A�\)A�A�A�A�\)A��A�A�A���A�A�G�B  B&��B'�dB  BQ�B&��B+B'�dB*2-A\  Ab{A\ �A\  Al��Ab{AH�RA\ �Ac�hA�AC�A��A�A�DAC�A��A��A�@�     DtnDsʇDr�A�z�AԴ9A��A�z�A�\AԴ9A�E�A��A�ƨB\)B#�PB$B\)B��B#�PB�uB$B&��A]��Ac��A\bNA]��Am��Ac��AJ5@A\bNAd��A zAU	A)}A zA|sAU	A��A)}A��@�B�    DtQ�Ds��Dr�A���A�bNA�C�A���A�A�bNA�p�A�C�A��TB  B H�B!/B  B��B H�BK�B!/B#��Ab�RAh�uAa?|Ab�RAs
>Ah�uAN��Aa?|AiK�Ak�A��An�Ak�A �A��A��An�A��@�     DtAHDs�jDr��A���A� �A�ZA���A�\A� �Aߣ�A�ZA�  B�\BW
BjB�\B  BW
B
VBjB!oA`��AjbNAbv�A`��Ap(�AjbNAP(�Abv�AjĜA3�A�AFA3�AGOA�A�1AFA��@�#�    Dt&gDs��Dr�!A�{A�(�A�$�A�{A���A�(�A�DA�$�A��B	��BB�B	��B�
BB��B�Bn�A_�Aj��Abr�A_�ApQ�Aj��AO7LAbr�Ak&�A�A
ASEA�As�A
A�ASEA=@�     Ds�)DsY�Drc�A�33A�(�A��A�33A��\A�(�A�hA��A��B33B�!B��B33B	�B�!B�?B��BG�A]��Aj��AcG�A]��At  Aj��AO+AcG�Ak|�AD4Ap
A��AD4A�eAp
A)�A��Af�@��    Ds��Ds7DrAA�33A�/A�A�33A���A�/A���A�A�33B\)BO�B%B\)B�RBO�B\B%B� Aa�AohrAf��Aa�At��AohrAS�Af��Aol�A��At�A��A��A�PAt�A��A��A@�u     Ds��Ds�Dr))A��A�+A�9A��A�=qA�+A�ƨA�9A�wB�
B�B�BB�
B��B�B DB�BBN�A]�Ao�AgS�A]�Ar=qAo�AR��AgS�Ap^6A�yA�%A�nA�yA��A�%A��A�nA��@��    Ds��Ds Dr�A�{A��A�A�{A�=qA��A�  A�A�33B33B��BŢB33B(�B��A���BŢB�A[�An^5Ae��A[�Av�\An^5AQ��Ae��AoVAA�ZA��AA�A�ZA1"A��A��@�V     Ds��Dr�TDq��A�ffA���A��A�ffA�
=A���A�-A��A� �B��B�BH�B��B��B�BjBH�B�Ab{Aq��Ai�hAb{Aw�
Aq��AU�Ai�hAs`BAuFAAd�AuFA �EAA
��Ad�A��@�ƀ    Ds|(DrڕDq�*A���A�A�K�A���A���A�A��A�K�A韾B�
B	7B�B�
B
��B	7B  B�BaHAb�HAvjAo�Ab�HAxQ�AvjA[&�Ao�Ax��A	=A R�A �A	=A!'�A R�AT�A �A"q�@�7     DshRDrƹDq�LA�\A�l�A��A�\A��A�l�A읲A��A�K�B
=B(�B��B
=B(�B(�B2-B��B!P�AnffA��A|1'AnffA��A��Af�\A|1'A�p�A��A'��A$ۏA��A(H�A'��A�SA$ۏA*�f@���    DsVfDr��Dq��A�ffA�7LA�jA�ffA�
=A�7LA���A�jA���B
=B#u�B$�PB
=B�B#u�B�B$�PB&�BAr�RA�"�A}�vAr�RA���A�"�Ah~�A}�vA��wA��A*�A%��A��A,�A*�A8�A%��A,l