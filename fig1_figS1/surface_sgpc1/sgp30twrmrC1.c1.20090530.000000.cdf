CDF   0   
      time             Date      Sun May 31 05:46:20 GMT 2009       Version       "$State: process-vap-twrmr-1.5-1 $      IDL_Tools_Lib_Version         &$State: ds-idltools-idltools-1.18-0 $      IDL_Version       .VAP coded in IDL (sparc / sunos / unix / 5.4)      Command_Line      */apps/process/bin/twrmr -g 30 -d 20090530      Number_Input_Platforms        5      Input_Platforms       Vsgp30smosE13.b1, sgpthwapsC1.b1, sgp30twr10xC1.b1, sgp30twr25mC1.b1, sgp30twr60mC1.b1      Averaging_Int         30 min     Surface_selection_order       6SMOS used as primary surface obs, THWAPS as secondary      Tower_selection_order         ?SE elevator used as primary tower obs, W elevator as secondary     zeb_platform      sgp30twrmrC1.c1    missing-data      ******        ,   	base_time                string        30-May-2009,00:00:00 GMT       	long_name         Base time in Epoch     units         $seconds since 1970-1-1 0:00:00 0:00         �   time_offset                 	long_name         Time offset from base_time     units         &seconds since 2009-5-30 00:00:00 0:00           �   pres_02m                	long_name         Barometric pressure at 2 m     units         mb             pres_25m                	long_name         Barometric pressure at 25 m    units         mb             pres_60m                	long_name         Barometric pressure at 60 m    units         mb             temp_02m                	long_name         Temperature at 2 m     units         degC               temp_25m                	long_name         Temperature at 25 m    units         degC               temp_60m                	long_name         Temperature at 60 m    units         degC               temp_02m_SMOS                   	long_name         !Temperature at surface from SMOS       units         degC               temp_02m_THWAPS                 	long_name         #Temperature at surface from THWAPS     units         degC                temp_25m_SE                 	long_name         %Temperature at 25 m from SE elevator       units         degC            $   
temp_25m_W                  	long_name         $Temperature at 25 m from W elevator    units         degC            (   temp_60m_SE                 	long_name         %Temperature at 60 m from SE elevator       units         degC            ,   
temp_60m_W                  	long_name         $Temperature at 60 m from W elevator    units         degC            0   rh_02m                  	long_name         Relative humidity at 2 m       units         %           4   rh_25m                  	long_name         Relative humidity at 25 m      units         %           8   rh_60m                  	long_name         Relative humidity at 60 m      units         %           <   rh_02m_SMOS                 	long_name         'Relative humidity at surface from SMOS     units         %           @   rh_02m_THWAPS                   	long_name         )Relative humidity at surface from THWAPS       units         %           D   	rh_25m_SE                   	long_name         +Relative humidity at 25 m from SE elevator     units         %           H   rh_25m_W                	long_name         *Relative humidity at 25 m from W elevator      units         %           L   	rh_60m_SE                   	long_name         +Relative humidity at 60 m from SE elevator     units         %           P   rh_60m_W                	long_name         *Relative humidity at 60 m from W elevator      units         %           T   vap_pres_02m                	long_name         Vapor pressure at 2 m      units         mb          X   vap_pres_25m                	long_name         Vapor pressure at 25 m     units         mb          \   vap_pres_60m                	long_name         Vapor pressure at 60 m     units         mb          `   vap_pres_02m_SMOS                   	long_name         $Vapor pressure at surface from SMOS    units         mb          d   vap_pres_02m_THWAPS                 	long_name         &Vapor pressure at surface from THWAPS      units         mb          h   vap_pres_25m_SE                 	long_name         (Vapor pressure at 25 m from SE elevator    units         mb          l   vap_pres_25m_W                  	long_name         'Vapor pressure at 25 m from W elevator     units         mb          p   vap_pres_60m_SE                 	long_name         (Vapor pressure at 60 m from SE elevator    units         mb          t   vap_pres_60m_W                  	long_name         'Vapor pressure at 60 m from W elevator     units         mb          x   mixing_ratio_02m                	long_name          Water vapor mixing ratio at 2 m    units         g/kg            |   mixing_ratio_25m                	long_name         !Water vapor mixing ratio at 25 m       units         g/kg            �   mixing_ratio_60m                	long_name         !Water vapor mixing ratio at 60 m       units         g/kg            �   mixing_ratio_02m_SMOS                   	long_name         .Water vapor mixing ratio at surface from SMOS      units         g/kg            �   mixing_ratio_02m_THWAPS                 	long_name         0Water vapor mixing ratio at surface from THWAPS    units         g/kg            �   mixing_ratio_25m_SE                 	long_name         2Water vapor mixing ratio at 25 m from SE elevator      units         g/kg            �   mixing_ratio_25m_W                  	long_name         1Water vapor mixing ratio at 25 m from W elevator       units         g/kg            �   mixing_ratio_60m_SE                 	long_name         2Water vapor mixing ratio at 60 m from SE elevator      units         g/kg            �   mixing_ratio_60m_W                  	long_name         1Water vapor mixing ratio at 60 m from W elevator       units         g/kg            �   lat              	long_name         north latitude     units         degrees         �   lon              	long_name         east longitude     units         degrees         �   alt              	long_name         	altitude       units         meters above Mean Sea Level         �J w Bk����RC�          Dt� Ds�/Dr�A�=qA���A�A�A�=qA��
A���A�&�A�A�A���A���AøRA��A���A�AøRA��mA��A�dZA#�A-S�A%�A#�A2�HA-S�A�$A%�A*ě@�f)@߆/@լ�@�f)@�)�@߆/@�na@լ�@��@�      Dt~�Ds��Dr�A�\)A��#A�\A�\)A�ffA��#A��wA�\A��A�z�A��A�Q�A�z�AӮA��A��wA�Q�A���A*zA.ĜA'hsA*zA>�RA.ĜA	��A'hsA-&�@ڸ�@�hI@ج�@ڸ�@��N@�hI@�*@ج�@�11@�      Dt��Ds�]Dr��A�G�A�A�l�A�G�A�\A�A�VA�l�A��
A��A��
A�A��A�p�A��
A�~�A�A�Q�A6�\A2JA%��A6�\AMp�A2JAȴA%��A+`B@��@�|@�Y�@��AbP@�|@��g@�Y�@�؅@�     Dt��Ds�Dr�A���A�v�A��^A���A֣�A�v�A�\)A��^A�FA�  A��mA�ZA�  B�\A��mA��yA�ZA�ȳA0(�A2=pA'C�A0(�AMp�A2=pA.IA'C�A-�@��@��|@�r�@��A_&@��|@��M@�r�@��@�      Dt��Ds�0Dr�tA��A�A�A��A�{A�A�;dA�A�^B�HA��A�E�B�HB0��A��A��9A�E�A��TA333A2��A*A333AS34A2��ACA*A.� @扁@�h@��@扁A"B@�h@��;@��@�+7@��     Dt��Ds�Dr�A�Q�A�hsA�ffA�Q�A���A�hsA�|�A�ffA�hsB>�BA��B>�BE{BA�VA��A�bAO�
AL�\A6r�AO�
A\��AL�\A4  A6r�A;|�A�A&�@�Y�A�AN�A&�@�2�@�Y�@��\@�     Dt��Ds�Dr�A�Q�Aԕ�A�+A�Q�A���Aԕ�A�oA�+A�XB1p�B:^A�VB1p�BL�\B:^BXA�VA�WAJ�HAKO�A2��AJ�HAZ�HAKO�A4�CA2��A6��A��AQ�@�D�A��A#�AQ�@���@�D�@���@Ȝ     Dt��Ds�Ds�A���A�z�A��`A���A��RA�z�A�r�A��`A�|�BNG�B B�NBNG�Ba�B B �B�NB�jAP��AO�
A=/AP��A]AO�
A9�<A=/AB �A�A?@�A�A�cA?@�Ŕ@�@���@�      Dt�|DtMDs�A�  A�XA��`A�  A��A�XA��
A��`A�z�BQB.ffB(�BQBv\(B.ffB2-B(�B��APz�AS�^A@M�APz�A_33AS�^A;�^A@M�AD�:AD�A��@��AD�A�A��@�!�@��@��k@Ϥ     Dt�=Dt�Ds`A��\A�n�A�\)A��\A�  A�n�A��!A�\)A��B_�
B7�bB�ZB_�
B��\B7�bB"gmB�ZB�AQ�AVȴA@ffAQ�A_�AVȴA<��A@ffAD�/A1�A
��@�9�A1�AJJA
��@�s@�9�@�@є     Dt�|DtDs�A��A��+A��A��A�  A��+A�t�A��A�ȴB~
>BG1'B��B~
>B�
=BG1'B0�B��B%O�AV�RA[��ADȴAV�RA_33A[��AB1ADȴAI\)A
W�A�(@�wA
W�A�A�(@�^�@�wA�:@�V     Dt�|Dt�Ds�A���A���A�33A���A��RA���A��A�33A��uB���BME�B��B���B���BME�B6�%B��B$�XAU��AZbA@�AU��A]�AZbAAnA@�AE\)A	�cA��@��A	�cA�gA��@��@��@���@�     Dt��Dt
&Ds2A�{A�E�A��A�{A�G�A�E�A���A��A�ƨB}G�BN�B'B�B}G�B��BN�B7�B'B�B.AQ�AU��ADr�AQ�AZffAU��A>�GADr�AI�A��A
;y@��:A��A��A
;y@�C�@��:AV_@��     Dt�*Dt�Ds�A�(�A��jA��+A�(�A���A��jA�oA��+A�~�Bq�BXR�B6�LBq�Bq�RBXR�B@F�B6�LB;�;AQG�A[��AM��AQG�AZffA[��AB=pAM��AP��A��A��A�A��A�A��@��A�AG�@؜     Dt�4Ds��Dr�A�(�A�l�A�dZA�(�A��
A�l�A�E�A�dZA�hsBl��B]o�BB�yBl��Br  B]o�BEhsBB�yBG�AP(�A[;eAM�AP(�AW�A[;eAC34AM�AP^5A�A�AA�A.A�@���AA'L@�^     Dt�>Ds�sDr��A�(�A���A�=qA�(�A�A���A��FA�=qA�K�Bh=qBZ<jB;B�Bh=qBiQ�BZ<jBBQ�B;B�B@�{AI�ASACO�AI�AO�
ASA<ffACO�AGl�A�eA��@�1GA�eA�0A��@�^@�1GAK�@�      Dt�pDs�Dr�A��
A�A�ffA��
A���A�A���A�ffA��Bj��BU�FB:1Bj��Bc  BU�FB>^5B:1B?�!AD��ANbA?S�AD��AH��ANbA5�#A?S�AAx�@�@?A#@�\@�@?Aq�A#@꟮@�\@���@��     Dt�Ds��Dr��A�p�A��A���A�p�A�  A��A��A���A�dZBl�B\�\B9r�Bl�Blz�B\�\BD49B9r�B?PAB�\ALn�A<n�AB�\AIG�ALn�A5G�A<n�A>�C@��vA:@�@N@��vA�vA:@��@�@N@��@ߤ     Dt~�Ds�DrסA�\)A��7A�`BA�\)A��A��7A�\)A�`BA�
=B�\Be��B<P�B�\B~ffBe��BM�_B<P�BA��AD��ALr�A;��AD��AJ�HALr�A6��A;��A=��@�O�A�@�p�@�O�A�DA�@��@�p�@�Q.@�     Dt~�Ds՞Dr��A~=qA�n�A�~�A~=qA�{A�n�A��A�~�A��PB�B�Bn�hB<hB�B�B�G�Bn�hBV�B<hBB%AD��AMl�A;�FAD��AK
>AMl�A8  A;�FA=O�@�O�A��@�V�@�O�A�A��@�{�@�V�@�p#@�     Dt�3Ds��Dr�Aw33A���A�XAw33A~�RA���A���A�XA��B�ffBy9XB;o�B�ffB���By9XB`�B;o�BAH�AD��APM�A<Q�AD��AL��APM�A:jA<Q�A>A�@���A�@��@���A��A�@�@��@��@�u     Dt��Ds�,DrشAvffA��A�Q�AvffA�  A��A�ZA�Q�A�`BB���B|��B:F�B���B�33B|��BeiyB:F�B@t�AD��AO�A>  AD��AK�AO�A:�A>  A?�@��Ab,@�Us@��A>�Ab,@�2�@�Us@�ڬ@�V     Dt~Ds�uDr�	Ao�
A�XA�A�Ao�
At��A�XA��A�A�A�;dB���B{k�B8x�B���B�33B{k�Bc� B8x�B>%�AD��AL��A>�AD��AK
>AL��A9�PA>�A@V@��;AP�@��m@��;A�[AP�@�@��m@�iD@�7     Dt�
Ds�(DrݵAg�
A��/A��RAg�
AqA��/A��uA��RA�K�B�  B��hBI�(B�  B���B��hBi�bBI�(BM��AC34AMƨABAC34AJ�RAMƨA:�ABAC��@�f&A�b@���@�f&A�A�b@�6�@���@���@�     Dt�{Ds�pDr��Ag�A��^A�l�Ag�Az�HA��^A�n�A�l�A���B�  B� �BK��B�  B�ffB� �Bn��BK��BO��ADz�AM��ABM�ADz�AO33AM��A:�ABM�ACX@�rA�F@���@�rA�eA�F@�?�@���@�Fz@��     Dt�)Ds�%Dr�"Av�\A��A�VAv�\A��RA��A��A�VA�p�B���B�M�Bey�B���B�33B�M�B} �Bey�Bd/AJ=qAQ�7AF��AJ=qATQ�AQ�7A>�:AF��AEA?mAbA �#A?mA�+Ab@�!:A �#A 5�@��     Dt�fDs�EDr�A���A��-A�VA���A���A��-A���A�VA���B���B�o�B_�SB���Bj��B�o�Boz�B_�SB`DAO\)AVfgAIp�AO\)AY��AVfgAC��AIp�AIx�A�A
��A�{A�AJ�A
��@���A�{A��@�     Dt��Ds�Dr�A�=qA���A�+A�=qA��A���A��A�+A���Bi��BnL�Bey�Bi��BU�BnL�BR�/Bey�Be�AZ�HAc�A[�TAZ�HAh  Ac�AJ�RA[�TA\��AEAo�A��AEA�_Ao�A��A��A[I@�     Dt�>Ds��Dr�A�=qA�VA���A�=qA�z�A�VA���A���A�=qB[(�Bc��Ba�=B[(�BH�\Bc��BG}�Ba�=Ba�HAh  Ap��Ajr�Ah  As�Ap��AUAjr�Ak�PA��AǃAX�A��Aw�AǃA
%pAX�A@�}     Dt��Ds�KDr�KA�z�A��hA���A�z�A�z�A��hA���A���A�$�BI�BP!�BP�BI�B3z�BP!�B6BP�BQ�AqG�A{
>As�_AqG�A�ffA{
>A\�*As�_Av�/A��A"�gA|	A��A%�eA"�gA�A|	A �~@�^     Dt��Ds�kDr�A�p�A�?}A�
=A�p�A�p�A�?}A�;dA�
=A��HB1p�B5Q�B5ĜB1p�B!�B5Q�B�B5ĜB8�Ap��Au�An��Ap��A}p�Au�AWAn��As��A�6AI�AY�A�6A#�=AI�AxAY�Aa�@�?     Dt��Ds�Dr��A�=qAڅA��;A�=qA�AڅA��A��;A�/B
=B$/B$��B
=B��B$/BQ�B$��B(Ai�AnffAg�Ai�At��AnffAS��Ag�AnbA -AX�An�A -A=�AX�A��An�A�$@�      Dt��Ds��Dr�8A�  A�A۲-A�  A��A�A��
A۲-A�&�B��B�qBaHB��B=qB�qB��BaHBA�Ab�HAg�<A]�^Ab�HAo�Ag�<AL� A]�^Af2Ag.A�A�Ag.AʌA�A=�A�AvS@�     Dts3DsЮDrٻA�\)A�ƨA�A�\)A���A�ƨA�1A�A�$�B33B2-B��B33A���B2-A�5?B��BF�AY�Ac\)AZA�AY�Ae��Ac\)AG��AZA�AbQ�A�A&8A�A�A:�A&8A ��A�A�@��     Dtg
Ds��DrΚA�p�A��A�wA�p�B  A��A�A�wA�v�A��HBbNB\A��HA�z�BbNA�7LB\B
.AW\(A`��AX  AW\(Ai�A`��ADIAX  A_�_A
�JAv�AH0A
�JAAv�@�L�AH0A_�@��     DtU�Ds��Dr�-A�G�A�A�A�A�G�B�\A�A�A��A�A�jA��B�+B�TA��A�zB�+A��yB�TB+A[�
Ab��AZ�]A[�
A\��Ab��ADI�AZ�]Ab~�A��A�A�A��A��A�@��+A�A=+@�     Dt@ Ds��Dr�SA�{A�~�A�oA�{B�
A�~�A���A�oA�wAA��RA�1AA�A��RA�?|A�1A�`BAW\(A]|�AU"�AW\(AeA]|�A=��AU"�A\(�A�Ai6A
z�A�AuAi6@�ECA
z�A@�B�    Dt*�Ds��Dr��A�
=A��A���A�
=B
=A��A�ZA���A���Aߙ�A�~�A�PAߙ�A��A�~�A��`A�PA�&�AM�AQ�mAJ$�AM�AN=qAQ�mA0-AJ$�AP��A^�AڪAI�A^�A-Aڪ@��AI�A��@�     Dt�Dst�Dr��A�
=A���A�z�A�
=B  A���A��A�z�A���AˮA�\)A��AˮA���A�\)A��A��A��A@Q�AF~�A>zA@Q�AD��AF~�A�>A>zAD~�@�%A k3@���@�%@��A k3@�c�@���@�:�@�#�    Dt  Ds_�Drk�B�B A��7B�B	�\B B�#A��7B z�A�\)A�1'A��#A�\)A���A�1'A�A�A��#A��A8(�A9�,A2Q�A8(�A=�A9�,A��A2Q�A8M�@퇛@�(�@�l�@퇛@��@�(�@��
@�l�@�H�@�     Ds��DsL�DrYOBp�B5?B ��Bp�BffB5?Bn�B ��BoA���A�A��/A���A��
A�A��\A��/A���A0��A.I�A&�jA0��A.|A.I�A7�A&�jA*�@��N@�N�@�K
@��N@�s�@�N�@���@�K
@��
@��    Ds��Ds3�DrAB=qBk�B�)B=qB�
Bk�B�bB�)B �A���A��wA��wA���A�Q�A��wA��TA��wA�bA.=pA.��A'�A.=pA8��A.��A��A'�A+hs@��O@��@�ܐ@��O@�S�@��@�L�@�ܐ@ނt@�u     Ds��Ds�Dr,/B�BDB"�B�B��BDBE�B"�B�A���A�{A��A���A���A�{A��A��A���A4��A9�TA0�HA4��A:=qA9�TAT�A0�HA5�<@��@�@���@��@��@�@��@���@�U�@��    Ds��Ds2Dr�B  Bq�B8RB  B�Bq�BD�B8RB5?A��A�A���A��A��\A�A�^5A���A�l�A9A=K�A4�A9ABfgA=K�A��A4�A:5@@��@�3A@���@��@�>@�3A@��@���@�O@�V     Ds��DsDr�B�RBr�B�^B�RB=qBr�B;dB�^Bz�A�G�A�S�A���A�G�A�Q�A�S�A���A���A�A�A3
=A5��A-�A3
=A5�A5��A��A-�A2Ĝ@�2�@�j@��+@�2�@��@�j@�C�@��+@�[�@�ƀ    Ds��Dr�{Dr ]B33B�?B\B33B  B�?BS�B\B��A�z�A��yA�(�A�z�A�G�A��yA�$�A�(�A��\A.�HA1�A(��A.�HA8  A1�A-wA(��A.5?@���@�jE@ی�@���@���@�jE@�c@ی�@�l�@�7     Ds�HDr��Dq��B�HB
=BPB�HB��B
=B�bBPB�A��
A�ffA�^5A��
A��HA�ffA�I�A�^5A���A/\(A8��A/\(A/\(A8��A8��A�A/\(A4�k@��@�3�@� "@��@���@�3�@��L@� "@��@���    Dst|Dr��Dq�zBB�
B��BBp�B�
B��B��B�NA�A��/A���A�A��A��/A��#A���A�"�A>=qAL9XAC�^A>=qAP��AL9XA!�8AC�^AI�8@�SA��@���@�SA-2A��@�U@���AE#