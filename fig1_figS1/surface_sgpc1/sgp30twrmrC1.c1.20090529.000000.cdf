CDF   0   
      time             Date      Sat May 30 05:47:27 GMT 2009       Version       "$State: process-vap-twrmr-1.5-1 $      IDL_Tools_Lib_Version         &$State: ds-idltools-idltools-1.18-0 $      IDL_Version       .VAP coded in IDL (sparc / sunos / unix / 5.4)      Command_Line      */apps/process/bin/twrmr -g 30 -d 20090529      Number_Input_Platforms        5      Input_Platforms       Vsgp30smosE13.b1, sgpthwapsC1.b1, sgp30twr10xC1.b1, sgp30twr25mC1.b1, sgp30twr60mC1.b1      Averaging_Int         30 min     Surface_selection_order       6SMOS used as primary surface obs, THWAPS as secondary      Tower_selection_order         ?SE elevator used as primary tower obs, W elevator as secondary     zeb_platform      sgp30twrmrC1.c1    missing-data      ******        ,   	base_time                string        29-May-2009,00:00:00 GMT       	long_name         Base time in Epoch     units         $seconds since 1970-1-1 0:00:00 0:00         �   time_offset                 	long_name         Time offset from base_time     units         &seconds since 2009-5-29 00:00:00 0:00           �   pres_02m                	long_name         Barometric pressure at 2 m     units         mb             pres_25m                	long_name         Barometric pressure at 25 m    units         mb             pres_60m                	long_name         Barometric pressure at 60 m    units         mb             temp_02m                	long_name         Temperature at 2 m     units         degC               temp_25m                	long_name         Temperature at 25 m    units         degC               temp_60m                	long_name         Temperature at 60 m    units         degC               temp_02m_SMOS                   	long_name         !Temperature at surface from SMOS       units         degC               temp_02m_THWAPS                 	long_name         #Temperature at surface from THWAPS     units         degC                temp_25m_SE                 	long_name         %Temperature at 25 m from SE elevator       units         degC            $   
temp_25m_W                  	long_name         $Temperature at 25 m from W elevator    units         degC            (   temp_60m_SE                 	long_name         %Temperature at 60 m from SE elevator       units         degC            ,   
temp_60m_W                  	long_name         $Temperature at 60 m from W elevator    units         degC            0   rh_02m                  	long_name         Relative humidity at 2 m       units         %           4   rh_25m                  	long_name         Relative humidity at 25 m      units         %           8   rh_60m                  	long_name         Relative humidity at 60 m      units         %           <   rh_02m_SMOS                 	long_name         'Relative humidity at surface from SMOS     units         %           @   rh_02m_THWAPS                   	long_name         )Relative humidity at surface from THWAPS       units         %           D   	rh_25m_SE                   	long_name         +Relative humidity at 25 m from SE elevator     units         %           H   rh_25m_W                	long_name         *Relative humidity at 25 m from W elevator      units         %           L   	rh_60m_SE                   	long_name         +Relative humidity at 60 m from SE elevator     units         %           P   rh_60m_W                	long_name         *Relative humidity at 60 m from W elevator      units         %           T   vap_pres_02m                	long_name         Vapor pressure at 2 m      units         mb          X   vap_pres_25m                	long_name         Vapor pressure at 25 m     units         mb          \   vap_pres_60m                	long_name         Vapor pressure at 60 m     units         mb          `   vap_pres_02m_SMOS                   	long_name         $Vapor pressure at surface from SMOS    units         mb          d   vap_pres_02m_THWAPS                 	long_name         &Vapor pressure at surface from THWAPS      units         mb          h   vap_pres_25m_SE                 	long_name         (Vapor pressure at 25 m from SE elevator    units         mb          l   vap_pres_25m_W                  	long_name         'Vapor pressure at 25 m from W elevator     units         mb          p   vap_pres_60m_SE                 	long_name         (Vapor pressure at 60 m from SE elevator    units         mb          t   vap_pres_60m_W                  	long_name         'Vapor pressure at 60 m from W elevator     units         mb          x   mixing_ratio_02m                	long_name          Water vapor mixing ratio at 2 m    units         g/kg            |   mixing_ratio_25m                	long_name         !Water vapor mixing ratio at 25 m       units         g/kg            �   mixing_ratio_60m                	long_name         !Water vapor mixing ratio at 60 m       units         g/kg            �   mixing_ratio_02m_SMOS                   	long_name         .Water vapor mixing ratio at surface from SMOS      units         g/kg            �   mixing_ratio_02m_THWAPS                 	long_name         0Water vapor mixing ratio at surface from THWAPS    units         g/kg            �   mixing_ratio_25m_SE                 	long_name         2Water vapor mixing ratio at 25 m from SE elevator      units         g/kg            �   mixing_ratio_25m_W                  	long_name         1Water vapor mixing ratio at 25 m from W elevator       units         g/kg            �   mixing_ratio_60m_SE                 	long_name         2Water vapor mixing ratio at 60 m from SE elevator      units         g/kg            �   mixing_ratio_60m_W                  	long_name         1Water vapor mixing ratio at 60 m from W elevator       units         g/kg            �   lat              	long_name         north latitude     units         degrees         �   lon              	long_name         east longitude     units         degrees         �   alt              	long_name         	altitude       units         meters above Mean Sea Level         �J%�Bk����RC�          Ds�pDsI�DrP�A���A�+A�%A���A��HA�+A�|�A�%A��
A��HA�34AŮA��HA�  A�34A���AŮA��A�RA
q�A�A�RA��A
q�@���A�A��@�?@���@���@�?@��?@���@�?�@���@��@�      Ds��DsQ�DrX�A�p�Aд9AΗ�A�p�A�ffAд9Aϴ9AΗ�A�XA��A�VA�bMA��A�A�VA��A�bMA�G�A��A \A�oA��A!G�A \@�.A�oA��@���@���@���@���@Ͼ�@���@�
�@���@���@�      Dt�Ds__Dre�A��RA�l�A�1'A��RAˮA�l�A�=qA�1'A�=qA���A�j~A�ƧA���B 33A�j~A�{A�ƧA�	A��A��A��A��A&�HA��@�D�A��A��@���@�~D@�f�@���@���@�~D@�̋@�f�@�3�@�     Dt
Dsr)Drx!A��
A�  A�v�A��
A���A�  Aɗ�A�v�Aɏ\BA�7LA���BB(�A�7LA�K�A���A��xA��Ax�A0UA��A0��Ax�A�KA0UA��@���@Ĵ�@�^�@���@�p@Ĵ�@�� @�^�@��*@�      Dt&gDs�=Dr��A���A�/A�$�A���A��A�/A�/A�$�A�l�B�RA��DA�~�B�RB	�A��DA���A�~�A�n�AfgA��An/AfgAQ�A��A�An/A?@�?�@�^�@��@�?�@�"9@�^�@�n�@��@�=�@��     Dt2�Ds��Dr��A��A�E�A�$�A��A��A�E�A�A�$�A��Bp�B�bA��
Bp�B(  B�bA��/A��
A�A��A\�A�A��A#�A\�A	�YA�A��@���@�KE@�s/@���@���@�KE@�X�@�s/@���@�     DtC�Ds�fDr�4A�(�A�dZA�
=A�(�A���A�dZA�`BA�
=A���B0��B	��A��B0��Bk��B	��A�A��A���A$Q�A��A��A$Q�A:�HA��A	�)A��A*0@�o�@�t�@���@�o�@���@�t�@�y�@���@���@Ȝ     DtT{Ds�Dr�fA�(�A��A�A�(�A��\A��A��yA�A�r�B_�RB<�=B0�B_�RB�u�B<�=B&�wB0�B733A5AK�AA�A5AC
=AK�A2ȵAA�AF��@��AO@��@��@�eZAO@��O@��A
�@�      Dtl�Ds��Dr�+A���A���A�JA���A�  A���A�7LA�JA��HBuzBI6FB+\BuzBy�
BI6FB27LB+\B2t�A@Q�AO��A;�lA@Q�AJ�HAO��A7A;�lA@��@���A6�@�@���A��A6�@�@�@�@�~@Ϥ     Dtz�Ds��Dr��A��RA�bNA��A��RA�G�A�bNA��yA��A��Bz�
BO�B+P�Bz�
B���BO�B8H�B+P�B2�DAC\(AOO�A9l�AC\(AG33AOO�A7nA9l�A=�F@��<A��@�X$@��<A UqA��@�H^@�X$@���@є     Dt��Ds�qDr�|A�=qA�(�A�oA�=qA�ffA�(�A�`BA�oA���B�{BX�B(��B�{B��=BX�BA� B(��B0��AC�AQl�A7�"AC�AJ�\AQl�A9��A7�"A;��@��QA\ @�A@��QA�A\ @@�A@�:o@�V     Dt��Ds��DrقA{
=A��PA��yA{
=Aj{A��PA��jA��yA���B��fBX5>B-jB��fB���BX5>BA	7B-jB5�AC
=AN$�A:  AC
=AD��AN$�A6�`A:  A=l�@�7aA7�@��@�7a@��A7�@��@��@��@�     Dt��Ds�xDr݂Ad  A��yA�t�Ad  AV{A��yA�S�A�t�A�bNB�  B]"�B*�B�  B���B]"�BF<jB*�B3A>�GAK`BA6�9A>�GAB�HAK`BA5|�A6�9A:�y@�ŉAe�@�Z@�ŉ@���Ae�@�-x@�Z@�C7@��     Dt�Ds�EDr�A[33A�ƨA�VA[33AP(�A�ƨA���A�VA�E�B�  Bl�mB:�;B�  B���Bl�mBT�B:�;B@�A<  AJ�9A<��A<  A?�AJ�9A6~�A<��A>Z@��\A�@�u�@��\@���A�@�v�@�u�@��e@؜     Dt��Ds�Dr�AH(�A�jA��AH(�AF=qA�jA�Q�A��A��B�  B�"NBW�ZB�  B�  B�"NBx&BW�ZB\A733AJ��AE�A733A9�AJ��A8��AE�AE%@밼A�A R�@밼@�0�A�@�s�A R�@�w�@�^     Dt��Ds�'Dr�AN�\A��`A�hsAN�\AA��A��`A�9XA�hsA��B�  B��5Bf�B�  B���B��5B�-Bf�Bl>xA9��AKO�AHE�A9��A8��AKO�A:��AHE�AG�^@��IAQ"Aޒ@��I@�KAQ"@�ېAޒA�@�      Dt��Ds�6Dr��A^{A�VA��hA^{A7�A�VA���A��hA�ƨB���B�(sBbɺB���B�ffB�(sB}�BbɺBh2-A?
>AL$�AK7LA?
>A8��AL$�A:�`AK7LAJ@��A�A��@��@풌A�@�,�A��A�@��     Dt�RDs��Dr�AUp�A�;dA���AUp�AJ{A�;dA��A���A�;dB�  B�0!B]�+B�  B�33B�0!B���B]�+BcP�A=AM��AF�A=AD��AM��A<9XAF�AFz�@�,�A�#A ��@�,�@�$A�#@��DA ��A ��@ߤ     Dt��Ds��Dr�CAR�RAzz�A��HAR�RAO33Azz�AzZA��HA��DB�33B��yBb0 B�33B�33B��yB��Bb0 BghA>fgAK7LAFz�A>fgAEG�AK7LA;
>AFz�AE�P@��A;*A �B@��@��aA;*@�Q#A �BA E@�     Dt�*Dt �Dr��AJ�RAr�9A��mAJ�RA%�Ar�9At�A��mA�hsB���B�s3Bn�DB���B���B�s3B��oBn�DBsA�A=p�AHv�AF5@A=p�A/
=AHv�A9x�AF5@AD��@�SAi�A y�@�S@��Ai�@�=A y�@��D@�     Dt��DtWDs�AAp�Aq
=A��AAp�A!G�Aq
=ArJA��A�%B�33B�iyBg�B�33B�33B�iyB�� Bg�Bm�-AAG�AHr�AF�aAAG�A/�
AHr�A9;dAF�aAE�-@���A^�A �t@���@��LA^�@�ܤA �tA �@�u     Dt��Dt �Ds.A6�RAi�-A��mA6�RA&=qAi�-Aj�DA��mA�|�B�33B�J�Be�B�33B�33B�J�B�ܬBe�Bi��A<��AFjAA\)A<��A3�AFjA7�"AA\)A@A�@��zA +@�u@��z@��A +@��@�u@�o@�V     Dt�Dt)=Ds&�A1Am�-A��^A1A (�Am�-AlI�A��^A�Q�B���B��NBe8RB���B�ffB��NB���Be8RBjE�A6ffAHěA<��A6ffA2=pAHěA9S�A<��A;�F@�loA�:@�7�@�lo@��A�:@��@�7�@��@�7     Dt�
Dt;Ds8%A,��Am%A�A,��A,��Am%Am�A�A�hsB���B��Bc��B���B�  B��B�g�Bc��BgW
A3
=AH��A6A3
=A4(�AH��A9"�A6A5;d@��!A`>@�~~@��!@�q�A`>@�X@�~~@�w�@�     Dt�\DtSzDsP�A0(�Au33A�%A0(�A>=qAu33Aw�A�%A�B�ffB�ÖBgy�B�ffB�33B�ÖBy�>Bgy�Bj`BA4��A?�-A:�+A4��A?�A?�-A/VA:�+A9|�@�d�@�	F@�N�@�d�@�Tz@�	F@�Y�@�N�@���@��     Du�DtoDslKADz�Ah~�A�`BADz�Al(�Ah~�AmC�A�`BA��DB�  B���Bl�fB�  B�ffB���B���Bl�fBl��AA�AEA6��AA�AS\)AEA5XA6��A5X@��@��J@�T@��A�>@��J@�o]@�T@�jp@��     Du*>DtRDs}�Aj{Au;dA��`Aj{A�  Au;dAw�hA��`A�/B�33B�ۦBU�4B�33B�\)B�ۦB���BU�4BX� AK�ALn�A-33AK�AS\)ALn�A:=qA-33A-�A��A�W@ߴ�A��A�A�W@��@ߴ�@ߏQ@�     Du8�Dt�2Ds��A��
A�p�A���A��
A�
=A�p�A��A���A��B��B��DBP/B��Bb�B��DBo!�BP/BPAL��AOC�A.=pAL��AO33AOC�A;"�A.=pA/"�A�JA�@� �A�JA%�A�@��@� �@�,}@�     DuC4Dt��Ds�fA�G�A���A�E�A�G�A�  A���A��FA�E�A��BVQ�B]w�BPw�BVQ�BE33B]w�BA�NBPw�BR��A@��AF�9A;�<A@��APQ�AF�9A//A;�<A<�R@��a@��[@�ł@��aAڷ@��[@�A�@�ł@��x@�}     DuR�Dt�JDs��A�33A�?}A�K�A�33A��RA�?}A�=qA�K�A���B?��BCO�B<�\B?��B+��BCO�B*��B<�\BDJ�AC\(AH~�A@1'AC\(AG�
AH~�A-�"A@1'AB��@��2A@�Y�@��2A M�A@�xd@�Y�@�}�@�^     DuN�Dt�BDs��A���A��+A��7A���A���A��+A�A��7A���B+�B1B.B+�B��B1BhB.B2�bA@Q�AD~�A>M�A@Q�ADQ�AD~�A+|�A>M�AA�@�ش@��@��d@�ش@��@��@�gR@��d@���@�?     DuE�Dt�9Ds��A���A��A�^5A���A֏\A��A�v�A�^5A�
=B�
B��B�B�
B �B��BO�B�BuA3
=A7�A/�,A3
=A4��A7�A ��A/�,A5C�@�I@��@���@�I@���@��@�G�@���@��@�      DuEDt�7Ds��AΏ\AˋDA�+AΏ\A���AˋDA�  A�+A��TB{B��B��B{A�34B��A��_B��B��A+�A5��A-\)A+�A?\)A5��A�;A-\)A2Q�@��@骅@��-@��@��@骅@��v@��-@�?@�     DuA�Dt��Ds�/A�z�Aԙ�A�C�A�z�A�\Aԙ�A�M�A�C�A�ȴA�� B�TBVA�� A�{B�TA��BVB��A/�
A8�RA1�A/�
A<Q�A8�RA5�A1�A7;d@�?@��c@�j�@�?@�0@��c@�~�@�j�@쫫@��     DuB�Dt��Ds��A�
=A�%A�jA�
=A�G�A�%A�&�A�jA�1A���A���A�O�A���A�
=A���A՝�A�O�A��RA2=pA6ȴA/�PA2=pA1G�A6ȴA�6A/�PA5�@䜝@�!1@⡼@䜝@�]�@�!1@�6@⡼@�g�@��     Du:>Dt��Ds�`A��HAߩ�A��A��HA�Aߩ�A���A��A��TA��
A�VA�A��
Aә�A�VA��+A�A�;dA'33A/�^A'��A'33A2�HA/�^A��A'��A,Q�@�S�@���@�W�@�S�@�y@���@�"@�W�@�p�@�     Du*>Dt��Ds��A��
A��TAݩ�A��
A�(�A��TA⛦Aݩ�A�S�A��A�jA�
<A��AŮA�jA��A�
<A��A'�
A.��A&r�A'�
A,��A.��A/A&r�A,$�@�6w@��v@��4@�6w@ݤ�@��v@�av@��4@�DF@�B�    Du �Dt}�Ds��A���A�uA�jA���A�  A�uA���A�jA�PA�
=A�"�A�-A�
=A��
A�"�A�ƨA�-A���A'�
A-��A%VA'�
A0(�A-��A�)A%VA*��@�>�@�Mt@�(@�>�@�	�@�Mt@�0@�(@ܐ�@�     Du�Dto�DsxEA�(�A�
=A�S�A�(�A�ffA�
=A蕁A�S�A�M�A�feA���A�bNA�feA�z�A���A��!A�bNA��yA+34A.��A&��A+34A$��A.��AT�A&��A,n�@ۧ@�$�@�^�@ۧ@ӏ�@�$�@�u@�^�@޹1@�#�    Du�Dt_:Dsh1A�p�A��A�l�A�p�A�z�A��A�9XA�l�A�/A��HAա�A��TA��HA��HAա�A���A��TA�JA$��A.z�A&��A$��A(��A.z�AffA&��A+�-@Ӟ8@���@�7�@Ӟ8@��(@���@�Lr@�7�@���@�     Dt� DtM�DsV�A�G�A��#A��A�G�A��
A��#A�A��A��TA�=qAѕ�A̋CA�=qA�ffAѕ�A��A̋CA�A!��A/"�A&1'A!��A(z�A/"�AF�A&1'A+��@�R�@�y�@ֱy@�R�@�>�@�y�@���@ֱy@�@��    Dt�pDt;<DsD�A�Q�A�DA�jA�Q�A�z�A�DA��A�jA�9A�\)A��:AȓtA�\)A���A��:A��AȓtA΍PA'�
A1�A(I�A'�
A(Q�A1�A�;A(I�A-�@�z�@��@�~N@�z�@�,@��@��@�~N@��_@�u     Dt�pDt+HDs5A�Q�A�^A���A�Q�B �A�^A� �A���AA�(�A�M�A��TA�(�A�33A�M�A�^5A��TA�A!A+;dA#��A!A3\*A+;dA�A#��A(��@ϥM@܄@ӲW@ϥM@��@܄@��@ӲW@��@��    Dt�(Dt$Ds$1A�  A��A�dZA�  B�
A��A��^A�dZA�ȴA��
A�A�bNA��
A�(�A�A�VA�bNA�bNA   A.5?A'�A   A.fgA.5?A�PA'�A,=q@�k�@�t-@��@�k�@��@�t-@�C@��@���@�V     Dt��DtDsvA���A�ƨA�1A���B �A�ƨA�XA�1A��A�G�AŬA�ȴA�G�A��AŬA�
=A�ȴA�A"�\A1XA'�A"�\A+34A1XA
��A'�A.J@��@䘰@آ�@��@�c@䘰@�O]@آ�@�2@�ƀ    Dt�GDs��Ds
6A�p�A�`BA�A�p�B�A�`BA�(�A�A�A�=qA�A�A�\)A�=qA���A�A�A�-A�\)A�K�A=pA((�A�hA=pA!�A((�A�{A�hA%o@�	�@ثh@�z&@�	�@� V@ثh@��@�z&@�~�@�7     Dt�fDs��Dr�`A���A�v�A��A���B �A�v�A��`A��A���A���A��-A�A���A�{A��-A�hsA�A��`A
=A)+A"E�A
=A!�A)+AH�A"E�A'K�@�@�\@�ߘ@�@�	�@�\@�`�@�ߘ@�q@���    Dt�)Ds�\Dr��A�z�A��A�uA�z�B (�A��A�;dA�uA�VA���A�r�A��jA���A���A�r�A���A��jA� �AQ�A)\*A"�\AQ�A,z�A)\*A!-A"�\A'��@�ͯ@�N�@�I0@�ͯ@��@�N�@�4]@�I0@���