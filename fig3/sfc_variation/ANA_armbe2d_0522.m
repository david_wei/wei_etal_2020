time=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','time');
lat=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','lat');
lon=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','lon');
depth=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','depth');
temp=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','temp');
cloud_low=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','cloud_low');
latent_heat_flux=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','latent_heat_flux');
latent_heat_flux_mean=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','latent_heat_flux_mean');
sensible_heat_flux=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','sensible_heat_flux');
lat_aircraft=ncread('racoro.20090522.152715.nc','lat');
lon_aircraft=ncread('racoro.20090522.152715.nc','lon');
alt_aircraft=ncread('racoro.20090522.152715.nc','gps_alt');
figure
pcolor(lon(8:11),lat(3:9),squeeze(-latent_heat_flux(8:11,3:9,521))')
caxis([180,280])
colorbar
hold on
leg1=zeros(size(hour));
leg1(4785:6493)=1;% 10.78am to 11.26am
leg1(:)=1;
hp=plot(-lon_aircraft(leg1==1),lat_aircraft(leg1==1),'r');
hp.LineWidth=4;
% title('Surface latent heat flux, 11am LT, May 22')
xlabel('Lon')
ylabel('Lat')
% legend('Latent heat flux','Aircraft track')
% Change text size for presentation
set(gca,'fontsize',16)
box on
%%
figure
plot(squeeze(latent_heat_flux(1,1,:)))
hold on
plot(latent_heat_flux_mean)
%%
figure
scatter(-latent_heat_flux(1,1,:),-sensible_heat_flux(1,1,:))
%%
figure
scatter(temp(10,10,300:400),-latent_heat_flux(10,10,300:400))
%% Cloud fraction
figure
contourf(-lon,lat,cloud_low(:,:,521)')
colorbar
hold on
plot(lon_aircraft,lat_aircraft,'k')
title('Cloud fraction, 11am LT, May 22')
xlabel('Long')
ylabel('Lat')
legend('Cloud fraction','Aircraft track')
% Change text size for presentation
set(gca,'fontsize',15)
box on