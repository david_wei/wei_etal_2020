%% PAPER2_3d
time=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','time');
lat=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','lat');
lon=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','lon');
depth=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','depth');
temp=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','temp');
cloud_low=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','cloud_low');
latent_heat_flux=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','latent_heat_flux');
latent_heat_flux_mean=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','latent_heat_flux_mean');
sensible_heat_flux=ncread('sgparmbe2dgridX1.c1.20090501.000000.nc','sensible_heat_flux');
hour=ncread('racoro.20090522.152715.nc','hour');
lat_aircraft=ncread('racoro.20090522.152715.nc','lat');
lon_aircraft=ncread('racoro.20090522.152715.nc','lon');
alt_aircraft=ncread('racoro.20090522.152715.nc','gps_alt');
figure
% pcolor(lon(8:11),lat(3:9),squeeze(-latent_heat_flux(8:11,3:9,520))')
pcolor(lon,lat,squeeze(-latent_heat_flux(:,:,520))')
caxis([160,250])
hc = colorbar;
set(get(hc,'label'),'string','Latent heat flux (W/m^2)');
hold on
leg1=zeros(size(hour));
leg1(4785:6493)=1;% 10.78am to 11.26am
% leg1(:)=1;
hp=plot(-lon_aircraft(leg1==1),lat_aircraft(leg1==1),'r');
hp.LineWidth=4;
% title('Surface latent heat flux, 11am LT, May 22')
xlabel('Lon')
ylabel('Lat')
xlim([-98,-97])
ylim([36,37])
% legend('Latent heat flux','Aircraft track')
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_3d.png'])