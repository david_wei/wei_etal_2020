%%adapted from mapping_loaddata_bomex.m by ytian
addpath('/n/home07/ytian/toolbox');
%addpath(genpath('/n/home08/jinie/njm'));

fn='./OUT_LPDM/';
fn_save='./OUT_LPDM/';
%fn='../OUT_lpdm/BOMEX_';
%fn_save='../loaddata/loaddata_';

caseid='WB97_prob19600';

%%%% preset parameters
para.nx    =128;   para.ny    =128;
para.lpdm_num =1000; %lpdm_num in lpdm_mod.f90
para.nsub=1;
seg = para.nx *para.ny *para.lpdm_num /para.nsub;
Nt= 10800; ndt= 30;  % only Nt/ndt matters
%para.cf=1;  % xy coarsen factor
%para.cfz=1; % z coarsen factor
restart_nsub=1; restart_L=0;
nsub_buff=1;    

%level_st=20; level_ed=85; dL=1; len=length([level_st:dL:level_ed]); 
%%%% preset parameters

%%%%%%% load data
%%if exist('./restart.mat','file')==2
%%    load('restart.mat')
%%    nsub_buff=restart_nsub;
%%else
%%end

for nsub= restart_nsub:para.nsub
    fnn1=[fn,num2str(caseid),'_sub',num2str(nsub),'.xyz'];   
    fid1=fopen(fnn1);
    xyz=zeros(seg,Nt/ndt);
%   ez=zeros(seg,Nt/ndt+1);  %exact location of z
    
    for t=1:Nt/ndt+1
		t
        fread(fid1,1,'int32');
        xyz(:,t) =fread(fid1,seg,'int32');
        fread(fid1,1,'int32');
    end

%   for t=1:Nt/ndt+1
%        fread(fid2,1,'float32');
%        ez(:,t) =fread(fid2,seg,'float32');
%        fread(fid2,1,'float32');
%   end
    fclose(fid1);
    fnn=[fn_save,num2str(caseid),'_sub',num2str(nsub),'.mat'];
    save(fnn,'xyz','-v7.3'); 
%   fclose(fid2);
    
%   size(xyz)
%   xyz(:,1)=[];    % the first one is initial condition, there is no 3D to match it
%   ez(:,1)=[]; 

%% discarding useless particles, for pchan usage
%    z=decode_z(xyz);  zmin=min(z,[],2);  zmax=max(z,[],2); clear z;
%    % parcels that donot cross the layer between level_st and level_ed. 
%    % Note that the critiria if from level_st+1 and level_ed-1
%    % coarsen the resolution
%    L =level_st;
%    zmax=round(zmax/para.cfz+0.1); zmin=round(zmin/para.cfz+0.1);
%    %for L= level_st:dL:level_ed-1
%       %I=~( (zmax<level_st)|(zmin>level_ed) );
%       I=((zmax>=level_st) &(zmin<=level_ed));
%        xyzz=xyz(I,:);
%%       ezz=ez(I,:);
%  [nsub size(xyzz)]
%        fnn=[fn_save,num2str(caseid),'_sub',num2str(nsub),'.mat'];
%        save(fnn,'xyzz','-v7.3'); 
% %      fnn=[fn_save,num2str(caseid),'_sub',num2str(nsub),'_L',num2str(level_st),'.mat']
% %      save(fnn,'xyzz','-v7.3','ezz'); 
%        %save(fnn,'xyzz','-v7.3')
end  % end nsub cycle
   
%'mapping_track'

