%% PAPER2_3a
addpath('~/Documents/MATLAB/RACORO_new/otherdays_function/')
figure('position',[440   378   373   420])
fname='racoro.20090522.152715.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090522.001037.nc';
n_start=4749;
n_end=6456;
plot_samples_tmixqt_06212020(fname,fname_profile,n_start,n_end,[],[],'05/22/2009')
xlim([300,320])
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_3a.png'])
%% PAPER2_3b
figure('position',[440   378   373   420])
fname='RACORO_racoro_mb_128_0000012000.nc';
plot_samples_06212020(fname,[1,1,1024,128,66],'k');
xlim([305,315])
xlim([300,320])
ylim([0,15])
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_3b.png'])
%% PAPER2_3c
figure('position',[440   378   373   420])
fname='RACORO_racoro_mg_128_0000012000.nc';
plot_samples_06212020(fname,[1,1,1024,128,66],'k');
xlim([305,315])
xlim([300,320])
ylim([0,15])
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_3c.png'])