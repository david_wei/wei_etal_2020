%% PAPER2_3e
time=ncread('RACORO_racoro_mg_128_0000012000.nc','time');
p=ncread('RACORO_racoro_mg_128_0000012000.nc','p');
z=ncread('RACORO_racoro_mg_128_0000012000.nc','z');
qn=ncread('RACORO_racoro_mg_128_0000012000.nc','QN');
qn=qn/1000;
figure
plot(squeeze(mean(reshape(qn>0,[],length(z)))),z,'b')
hold on
xlabel('Cloud fraction')
ylabel('Altitude (m)')
qn=ncread('RACORO_racoro_mb_128_0000012000.nc','QN');
qn=qn/1000;
plot(squeeze(mean(reshape(qn>0,[],length(z)))),z,'r')
legend('Homogenous surface','Heterogeneous surface')
ylim([0,5000])
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_3e.png'])