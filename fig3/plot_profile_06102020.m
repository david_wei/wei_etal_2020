function [hp1,hp2]=plot_profile_06102020(fname,frame,cmapii)
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
x=ncread(fname,'x');
y=ncread(fname,'y');
z=ncread(fname,'z');
p=ncread(fname,'p');
w=ncread(fname,'W');
qn=ncread(fname,'QN');
qn=qn/1000;
qv=ncread(fname,'QV');
qv=qv/1000;
qt=qn+qv;
tabs=ncread(fname,'TABS');
pLEG=p(frame(5));
zLEG=z(frame(5));
alpha=0.272;

tabsP=squeeze(mean(mean(mean(tabs,1),2),4));
qvP=squeeze(mean(mean(mean(qv,1),2),4));
qnP=squeeze(mean(mean(mean(qn,1),2),4));
equiP=tabsP+(z+318)*9.81/1004+2.5e6/1004*qvP;

for Tv_tmp=tabsP(frame(5)).*(1+0.61*qvP(frame(5))-qnP(frame(5)))
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/pLEG(1)*18/29)-...
    max(0,q-es(T)/100/pLEG(1)*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:30e-3);
equi_iso=T_iso+(zLEG+318)*9.81/1004+2.5e6/1004*min(0.1e-3:0.1e-3:30e-3,...
    es(T_iso)/100/pLEG(1)*18/29);
hp1=plot(equi_iso*alpha+(1-alpha)*(T_iso+(zLEG+318)*9.81/1004-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:30e-3)-...
    es(T_iso)/100/pLEG(1)*18/29)),...
    (0.1e-3:0.1e-3:30e-3)*1000,'--','Color',cmapii,'Handlevisibility','off');
end

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
syms x;
INVes(x)=finverse(es(x));
hp2=plot((vpa(INVes((0:0.1e-3:30e-3)*29/18*100*pLEG))+(zLEG+318)*9.81/1004+...
    (0:0.1e-3:30e-3)*2.5e6/1004)*alpha+(1-alpha)*...
    (vpa(INVes((0:0.1e-3:30e-3)*29/18*100*pLEG))+...
    (zLEG+318)*9.81/1004),(0:0.1e-3:30e-3)*1000,'Color',cmapii,'Handlevisibility','off');

hold on

