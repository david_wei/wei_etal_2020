function [y]=decode_y(xyz)

y=floor((xyz-floor(xyz/1e6)*1e6)/1e3);
