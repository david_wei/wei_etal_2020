function [hss,hpp]=plot_samples_06202020(fname,frame,skip)

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
x=ncread(fname,'x');
y=ncread(fname,'y');
z=ncread(fname,'z');
p=ncread(fname,'p');
w=ncread(fname,'W');
qn=ncread(fname,'QN');
qn=qn/1000;
qv=ncread(fname,'QV');
qv=qv/1000;
qt=qn+qv;
tabs=ncread(fname,'TABS');
alpha=0.272;
V_max=10;
bluewhitered

tabsLEG=tabs(frame(1):frame(3),frame(2):frame(4),frame(5));
tabsLEG=tabsLEG(1:skip:end);
wLEG=w(frame(1):frame(3),frame(2):frame(4),frame(5));
wLEG=wLEG(1:skip:end);
qvLEG=qv(frame(1):frame(3),frame(2):frame(4),frame(5));
qvLEG=qvLEG(1:skip:end);
qnLEG=qn(frame(1):frame(3),frame(2):frame(4),frame(5));
qnLEG=qnLEG(1:skip:end);
qtLEG=qnLEG+qvLEG;
pLEG=p(frame(5));
zLEG=z(frame(5));

% figure
% SAM definition of \theta_{eq} and \theta_l
equiLEG=tabsLEG+(zLEG+318)*9.81/1004+2.5e6/1004*qvLEG;
tmixLEG=alpha*equiLEG+(1-alpha)*(tabsLEG+(zLEG+318)*9.81/1004-...
    2.5e6/1004*qnLEG);

hss=scatter(tmixLEG,qtLEG*1000,10,...
    cmap(min(101,max(1,int64(wLEG/V_max*50+6*sign(wLEG)+51))),:),'filled','handlevisibility','off');
hss.MarkerFaceColor=[0,0,0];
hss.MarkerEdgeColor=[0,0,0];
hss.MarkerFaceAlpha=0.6;
hss.MarkerEdgeAlpha=0.3;
xlim([305,320])
axis ij
hold on

tabsP=squeeze(mean(mean(mean(tabs,1),2),4));
qvP=squeeze(mean(mean(mean(qv,1),2),4));
qnP=squeeze(mean(mean(mean(qn,1),2),4));
equiP=tabsP+(z+318)*9.81/1004+2.5e6/1004*qvP;
hpp=plot(equiP*alpha+(1-alpha)*(tabsP+(z+318)*9.81/1004-2.5e6/1004*qnP),(qvP+qnP)*1000,'r','handlevisibility','off');
hpp=plot(equiP(1)*alpha+(1-alpha)*(tabsP(1)+(z(1)+318)*9.81/1004-2.5e6/1004*qnP(1)),(qvP(1)+qnP(1))*1000,'bo','handlevisibility','off');

% tmp=hist3([tmixLEG,qtLEG],'ctrs',{301:0.1:320,0:0.1e-3:12e-3})';
% pertmp=arrayfun(@(x)sum(tmp(tmp>x))/sum(tmp(:)),tmp);
% [C,h]=contour(301:0.1:320,(0:0.1e-3:12e-3)*1000,pertmp,[0.75,0.75],'k');
% xlim([306.5,313])
% clabel(C,h)

xlabel([num2str(alpha),'\theta_{e}+',num2str(1-alpha),'\theta_{l} (K)'])
ylabel('q_t (g/kg)')

% colormap(cmap)
cmap=ones(101,3);
cmap(52:101,2)=1-(1:50)/50;
cmap(52:101,3)=1-(1:50)/50;
cmap(50:-1:1,1)=1-(1:50)/50;
cmap(50:-1:1,2)=1-(1:50)/50;
% colormap(cmap)
% hc=colorbar;
% ylabel(hc,'Vertical velocity (m/s)')
% caxis([-V_max,V_max])
box on

% title(['An instance of Taylor and Baker (1991) (H=',num2str(z(frame(5))),' m)'])
% % Change text size for presentation
% set(gca,'fontsize',15)

for Tv_tmp=tabsP(frame(5)).*(1+0.61*qvP(frame(5))-qnP(frame(5)))
T_iso=arrayfun(@(q)fzero(@(T)T*(1+0.61*min(q,es(T)/100/pLEG(1)*18/29)-...
    max(0,q-es(T)/100/pLEG(1)*18/29))-Tv_tmp,300),0.1e-3:0.1e-3:30e-3);
equi_iso=T_iso+(zLEG+318)*9.81/1004+2.5e6/1004*min(0.1e-3:0.1e-3:30e-3,...
    es(T_iso)/100/pLEG(1)*18/29);
hp1=plot(equi_iso*alpha+(1-alpha)*(T_iso+(zLEG+318)*9.81/1004-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:30e-3)-...
    es(T_iso)/100/pLEG(1)*18/29)),...
    (0.1e-3:0.1e-3:30e-3)*1000,'k--','handlevisibility','off');
end

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
syms x;
INVes(x)=finverse(es(x));
hp2=plot((vpa(INVes((0:0.1e-3:30e-3)*29/18*100*pLEG))+(zLEG+318)*9.81/1004+...
    (0:0.1e-3:30e-3)*2.5e6/1004)*alpha+(1-alpha)*...
    (vpa(INVes((0:0.1e-3:30e-3)*29/18*100*pLEG))+...
    (zLEG+318)*9.81/1004),(0:0.1e-3:30e-3)*1000,'handlevisibility','off');
hp2.Color='k';
%
t_iso=equi_iso*alpha+(1-alpha)*(T_iso+(zLEG+318)*9.81/1004-...
    2.5e6/1004*max(0,(0.1e-3:0.1e-3:30e-3)-...
    es(T_iso)/100/pLEG(1)*18/29));
q_iso=0.1e-3:0.1e-3:30e-3;
tmixP=equiP*alpha+(1-alpha)*(tabsP+(z+318)*9.81/1004-2.5e6/1004*qnP);
tmix_prof=interp1(qvP,tmixP,q_iso);
idx_env=find((tmix_prof-t_iso)<=0,1,'first');
interp_env=(-tmix_prof(idx_env)+t_iso(idx_env))/(-tmix_prof(idx_env)+t_iso(idx_env)+tmix_prof(idx_env-1)-t_iso(idx_env-1));
% plot([tmix_prof(idx_env)*(1-interp_env)+tmix_prof(idx_env-1)*interp_env,tmixP(1)],[q_iso(idx_env)*(1-interp_env)+q_iso(idx_env-1)*interp_env,qvP(1)+qnP(1)]*1000)

%
q_shade=[q_iso(idx_env)*(1-interp_env)+q_iso(idx_env-1)*interp_env:1e-4:qvP(1)+qnP(1)];
t_prof=interp1(qvP+qnP,tmixP,q_shade);
t_iso=interp1(q_iso,t_iso,q_shade);
t_2p=interp1([q_iso(idx_env)*(1-interp_env)+q_iso(idx_env-1)*interp_env,qvP(1)+qnP(1)],[tmix_prof(idx_env)*(1-interp_env)+tmix_prof(idx_env-1)*interp_env,tmixP(1)],q_shade);
idx1=find((t_iso-t_2p)>0,1,'last');
idx2=find((t_iso-t_prof)>0,1,'last');
interp_1=(t_iso(idx1)-t_2p(idx1))/(t_iso(idx1)-t_2p(idx1)+t_2p(idx1+1)-t_iso(idx1+1));
interp_2=(t_iso(idx2)-t_prof(idx2))/(t_iso(idx2)-t_prof(idx2)+t_prof(idx2+1)-t_iso(idx2+1));
plot(polyshape([t_iso(idx1)*(1-interp_1)+t_iso(idx1+1)*interp_1,tmixP(1),t_prof(end:-1:(idx2+1)),t_prof(idx2)*(1-interp_2)+t_prof(idx2+1)*interp_2],[q_shade(idx1)*(1-interp_1)+q_shade(idx1+1)*interp_1,qvP(1)+qnP(1),q_shade(end:-1:(idx2+1)),q_shade(idx2)*(1-interp_2)+q_shade(idx2+1)*interp_2]*1000),'facecolor','k','facealpha',0.1,'handlevisibility','off')
plot(polyshape([t_iso(1:idx1),t_iso(idx1)*(1-interp_1)+t_iso(idx1+1)*interp_1,t_iso(1)],[q_shade(1:idx1),q_shade(idx1)*(1-interp_1)+q_shade(idx1+1)*interp_1,q_shade(1)]*1000),'facecolor','k','facealpha',0.1,'handlevisibility','off')

tabsP=squeeze(mean(mean(mean(tabs,1),2),4));
qvP=squeeze(mean(mean(mean(qv,1),2),4));
qnP=squeeze(mean(mean(mean(qn,1),2),4));
equiP=tabsP+(z+318)*9.81/1004+2.5e6/1004*qvP;
hpp=plot(equiP*alpha+(1-alpha)*(tabsP+(z+318)*9.81/1004-2.5e6/1004*qnP),(qvP+qnP)*1000,'r','handlevisibility','off','Linewidth',2);
hpp=plot(equiP(1)*alpha+(1-alpha)*(tabsP(1)+(z(1)+318)*9.81/1004-2.5e6/1004*qnP(1)),(qvP(1)+qnP(1))*1000,'bo','handlevisibility','off');
