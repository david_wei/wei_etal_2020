%% PAPER2_2a
fname='RACORO_racoro_cf_128_0000005920.nc';
plot_samples_06202020(fname,[1,41,25,65,60],1);
xlim([308,312])
ylim([0,12])
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_2a.png'])
%% PAPER2_2b
plot_samples_06102020('RACORO_racoro_cf_128_0000005920.nc',[54,1,75,24,62],'k')
% plot_samples_06102020('RACORO_racoro_cf_128_0000005920.nc',[54,1,75,24,60],1)
% plot_samples_06102020('RACORO_racoro_cf_128_0000005920.nc',[54,1,75,24,65],2)
% plot_samples_06102020('RACORO_racoro_cf_128_0000005920.nc',[54,1,75,24,70],3)
% plot_samples_06102020('RACORO_racoro_cf_128_0000005920.nc',[54,1,75,24,75],4)
% title('LES, 11 am')
% legend('show','location','best')
xlim([308,312])
ylim([0,12])
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_2b.png'])
%% PAPER2_2c
Transient_z_qt_INTERACT_03282020('racorocf_lpdm_1184.mat',[54,1,75,24,62],150)
% Change text size for presentation
set(gca,'fontsize',16,'box','on','Layer','top')
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_2c.png'])
%% Load processed data from Odyssey
load VAR_ANA2222_racorocf.mat
% %% PAPER2_2d
% % The lower branch
% hh=1:8;
% % figure('position',[100,100,1500,1500])
% figure
% plot(tt(102:end),mean(thetavlGqt0_l(hh,200:-1:1)-thetavlGqt0_l(hh,150),1),'b-','DisplayName','Lower: s_{vl}/c_p-s_{vl}(t = -1500 s)/c_p')
% hold on
% plot(tt(102:end),mean(-(thetavlMGqt0_l(hh,200:-1:1)-thetavlMGqt0_l(hh,150)),1),'r-','DisplayName','Lower: -(s_{vl}^{env}/c_p-s_{vl}^{env}(t = -1500 s)/c_p)')
% ylim([-5,5])
% plot(tt(102:end),mean(lam*qnGqt0_l(hh,200:-1:1),1),'m-','DisplayName','Lower: 2.05 q_l')
% plot(tt(102:end),mean(-dppGqt0_l(hh,200:-1:1),1),'k-','DisplayName','Lower: -dp/dz')
% title('4.1<q_t(t=0)<4.4g/kg & z(t=-800s)<2.5km, racorocf,[54,1,75,24,62]')
% xlabel('t (s)')
% ylabel('Buoyancy (k)')
% % The higher branch
% % figure
% hp=plot(tt(102:end),mean(thetavlGqt0_h(hh,200:-1:1)-thetavlGqt0_h(hh,150),1),'- b','DisplayName','Upper: s_{vl}/c_p-s_{vl}(t = -1500 s)/c_p','linewidth',3);
% hp.Color(4)=0.3;
% hold on
% hp=plot(tt(102:end),mean(-(thetavlMGqt0_h(hh,200:-1:1)-thetavlMGqt0_h(hh,150)),1),'- r','DisplayName','Upper: -(s_{vl}^{env}/c_p-s_{vl}^{env}(t = -1500 s)/c_p)','linewidth',3);
% ylim([-5,5])
% hp.Color(4)=0.3;
% hp=plot(tt(102:end),mean(lam*qnGqt0_h(hh,200:-1:1),1),'- m','DisplayName','Upper: 2.05 q_l','linewidth',3);
% hp.Color(4)=0.3;
% hp=plot(tt(102:end),mean(-dppGqt0_h(hh,200:-1:1),1),'- k','DisplayName','Upper: -dp/dz','linewidth',3);
% hp.Color(4)=0.3;
% title('')
% xlabel('t (s)')
% ylabel('Buoyancy (K)')
% ylim([-7,7])
% % legend('show','fontsize',10,'location','northwest')
% % Change text size for presentation
% set(gca,'fontsize',16)
% box on
%% PAPER2_2d
% The lower branch
hh=1:8;
% figure('position',[100,100,1500,1500])
figure
plot(tt(102:end),mean(thetavlGqt0_l(hh,200:-1:1)-thetavlGqt0_l(hh,150),1),'b-','DisplayName','$s_{vl}/c_p-s_{vl}(t = -1500 s)/c_p$')
hold on
plot(tt(102:end),mean(-(thetavlMGqt0_l(hh,200:-1:1)-thetavlMGqt0_l(hh,150)),1),'r-','DisplayName','$-s_{vl}^{env}/c_p+s_{vl}^{env}(t = -1500 s)/c_p$')
ylim([-5,5])
plot(tt(102:end),mean(lam*qnGqt0_l(hh,200:-1:1),1),'m-','DisplayName','$2.05\cdot q_l$')
plot(tt(102:end),mean(-dppGqt0_l(hh,200:-1:1),1),'k-','DisplayName','$-dp''/dz\cdot T_{v}/\rho g$')
title('4.1<q_t(t=0)<4.4g/kg & z(t=-800s)<2.5km, racorocf,[54,1,75,24,62]')
xlabel('t (s)')
ylabel('Buoyancy (k)')
% The higher branch
% figure
hp=plot(tt(102:end),mean(thetavlGqt0_h(hh,200:-1:1)-thetavlGqt0_h(hh,150),1),'- b','HandleVisibility','off','linewidth',5);
hp.Color(4)=0.3;
hold on
hp=plot(tt(102:end),mean(-(thetavlMGqt0_h(hh,200:-1:1)-thetavlMGqt0_h(hh,150)),1),'- r','HandleVisibility','off','linewidth',5);
ylim([-5,5])
hp.Color(4)=0.3;
hp=plot(tt(102:end),mean(lam*qnGqt0_h(hh,200:-1:1),1),'- m','HandleVisibility','off','linewidth',5);
hp.Color(4)=0.3;
hp=plot(tt(102:end),mean(-dppGqt0_h(hh,200:-1:1),1),'- k','HandleVisibility','off','linewidth',5);
hp.Color(4)=0.3;
title('')
xlabel('t (s)')
ylabel('Buoyancy (K)')
ylim([-7,7])
legend('show','fontsize',16,'location','northwest','interpreter','latex')
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_2d.png'])