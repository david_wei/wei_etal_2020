function Transient_z_qt_INTERACT_03282020(varargin)
fname_lpdm='racorocf_lpdm_1200.mat';
if (nargin>=1)
    if ~isempty(varargin{1})
        fname_lpdm=varargin{1};
    end
end
fname_lpdm0=fname_lpdm;
frame=[54,1,75,24,70];
if (nargin>=2)
    if ~isempty(varargin{2})
        frame=varargin{2};
    end
end
nback=5;
if (nargin>=3)
    if ~isempty(varargin{3})
        nback=varargin{3};
    end
end

z=ncread('RACORO_racoro_cf_128_0000002500.nc','z');
load(fname_lpdm,'xyz')
Lx0=decode_x(xyz);
Ly0=decode_y(xyz);
Lz0=decode_z(xyz);
Lx0(Lx0==129)=128;
Ly0(Ly0==129)=128;
Lz0(Lz0==129)=128;
Lisinframe=(Lx0>=frame(1))&(Lx0<=frame(3))&(Ly0>=frame(2))&(Ly0<=frame(4));

fname_3d=['RACORO_',insertAfter(strjoin(IndexableFunction(split(fname_lpdm,'_')){1:1},'_'),'racoro','_'),'_','128','_',...
                    num2str(str2num(IndexableFunction(split(fname_lpdm,{'_','.'})){3}{1})*5,'%010i'),'.nc'];
x0=ncread(fname_3d,'x');
y0=ncread(fname_3d,'y');
z0=ncread(fname_3d,'z');
p0=ncread(fname_3d,'p');
w0=ncread(fname_3d,'W');
qn0=ncread(fname_3d,'QN');
qn0=qn0/1000;
qv0=ncread(fname_3d,'QV');
qv0=qv0/1000;
qt0=qn0+qv0;
tabs0=ncread(fname_3d,'TABS');
shape0=size(qv0);

Lxyz0=sub2ind(size(qt0),Lx0,Ly0,Lz0);
Lqt0=qt0(Lxyz0);
Ltabs0=tabs0(Lxyz0);
zedges=(1:140)';
qtedges=(0.1:0.1:12)*1e-3;
Lisinframe1=(Lx0>=frame(1))&(Lx0<=frame(3))&(Ly0>=frame(2))&(Ly0<=frame(4))&(Lz0==frame(5));
% ptm1=hist3([Lzn(Lisinframe1),Lqt0(Lisinframe1)],'Edges',{edges,qtedges});%./...
%     repmat(reshape(histc(Lz0(Lisinframe1),edges),1,length(edges)),length(edges),1);





fname_lpdm=[strjoin(IndexableFunction(split(fname_lpdm,'_')){1:2},'_'),'_',...
                    num2str(str2num(IndexableFunction(split(fname_lpdm,{'_','.'})){3}{1})-nback),'.mat'];
load(fname_lpdm,'xyz')
Lzn=decode_z(xyz);
edges=(1:140)';
ptm=hist3([Lzn(Lisinframe),Lz0(Lisinframe)],'Edges',{edges,edges})./...
    repmat(reshape(histc(Lz0(Lisinframe),edges),1,length(edges)),length(edges),1);

ptm1=hist3([Lzn(Lisinframe1),Lqt0(Lisinframe1)],'Edges',{edges,qtedges})./...
    repmat(reshape(histc(Lqt0(Lisinframe1),qtedges),1,length(qtedges)),length(edges),1);
max(ptm1(:))
mean(ptm1(:))

fig=figure('KeyPressFcn',@frame_control);
% [~,hh]=contourf(z(1:100),z(1:101),ptm(1:101,1:100),150);
hh=pcolor(qtedges*1e3,z(1:100),ptm1(1:100,:)/40);
set(hh, 'EdgeColor', 'none');
% set(hh,'LineColor','none');
xlabel('q_t (t=0s) (g/kg)');
ylabel(['Height (t = -',num2str(nback*10),' s) (m)']);
% title(['Transient Matrix, tracking back by ',...
%     num2str(nback*10),'s, frame= [',num2str(frame),']']);

% xlabel('Height at t=0s (m)','Fontsize',20);
% ylabel('Height at t=-800s (m)','Fontsize',20);
% title('Transilient Matrix','Fontsize',20);

colorbar;
hold on
cmap=zeros(100,3);
cmap(:,3)=1;
cmap(:,2)=1:-0.01:0.01;
cmap(:,1)=1:-0.01:0.01;
hc=colormap(cmap);
caxis([0,0.01])
% plot(z(1:100),z(1:100),'r')
hc = colorbar;
set(get(hc,'label'),'string','PDF conditioned on q_t (m^{-1})');
% Change text size for presentation
% set(gca,'fontsize',20)

function frame_control(obj,evnt)
    switch evnt.Key
        case 'p'
            print(gcf,'-dpng','-r300',...
                ['Transient_z_qt_INTERACT_',fname_lpdm0,'_',...
                num2str(nback),'_',regexprep(num2str(frame),'\s+','-'),'.png'])
    end
end
end