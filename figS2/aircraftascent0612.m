%% File names
fname='racoro.20090612.180243.nc';
fname_profile='sgpaeri01prof3feltzC1.s1.20090612.001056.nc';
%%
hour=ncread(fname,'hour');
lat=ncread(fname,'lat');
lon=ncread(fname,'lon');
gps_alt=ncread(fname,'gps_alt');
pres=ncread(fname,'pres');
pres(pres<0)=nan;
vert_wind=ncread(fname,'vert_wind');
vert_wind(vert_wind<-100)=nan;
aircraft_east_vel=ncread(fname,...
    'aircraft_east_vel');
aircraft_north_vel=ncread(fname,...
    'aircraft_north_vel');
lwc_gerber=ncread(fname,'lwc_gerber');
specific_humid=ncread(fname,'specific_humid');
wvmr_dlh=ncread(fname,'wvmr_dlh');
wvmr_cr2=ncread(fname,'wvmr_cr2');
wvmr_edgetech=ncread(fname,'wvmr_dlh');
temperature=ncread(fname,'temperature');
equi_potential_temp=ncread(fname,...
    'equi_potential_temp');
rh=ncread(fname,'rh');
rho=(pres*100/287./(temperature+273.15));
cond=vert_wind>-100;
dist=sqrt((lat-36.605).^2+(lon-97.485).^2)*110;
%%
figure
plot(gps_alt-318)
hold on
alt_dist10=gps_alt;
alt_dist10(dist>10)=nan;
plot(alt_dist10-318,'r.')
%% 1
fname_sounding={'sgpsondeadjustC1.c1.20090612.052600.cdf','sgpsondeadjustC1.c1.20090612.113000.cdf',...
    'sgpsondeadjustC1.c1.20090612.172800.cdf','sgpsondeadjustC1.c1.20090612.232800.cdf',{'sgpsondeadjustC1.c1.20090613.052600.cdf',1}};
fname_aeri='sgpaeri01prof3feltzC1.c1.20090612.001056.cdf';
fname_aircraft='racoro.20090612.180243.nc';
n_start=588;
n_end=1807;
figure('position',[100,100,1500,400])
sgtitle('06/12 #1','fontsize',24)

subplot(1,2,1)
plot_rh_AircraftAERIBalloon(n_start,n_end,10,fname_sounding,fname_aeri,fname_aircraft)
xlim([0,100])
ylim([600,1000])
% legend('show','location','southwest','NumColumns',1,'fontsize',10)
% Change text size for presentation
set(gca,'fontsize',16)
box on

subplot(1,2,2)
plot_temperature_AircraftAERIBalloon(n_start,n_end,10,fname_sounding,fname_aeri,fname_aircraft)
xlim([220,240])
ylim([600,1000])
legend('show','location','northwest','NumColumns',1,'fontsize',10)
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_SUP2_24.png'])
%% 2
fname_sounding={'sgpsondeadjustC1.c1.20090612.052600.cdf','sgpsondeadjustC1.c1.20090612.113000.cdf',...
    'sgpsondeadjustC1.c1.20090612.172800.cdf','sgpsondeadjustC1.c1.20090612.232800.cdf',{'sgpsondeadjustC1.c1.20090613.052600.cdf',1}};
fname_aeri='sgpaeri01prof3feltzC1.c1.20090612.001056.cdf';
fname_aircraft='racoro.20090612.180243.nc';
n_start=10760;
n_end=11580;
figure('position',[100,100,1500,400])
sgtitle('06/12 #2','fontsize',24)

subplot(1,2,1)
plot_rh_AircraftAERIBalloon(n_start,n_end,10,fname_sounding,fname_aeri,fname_aircraft)
xlim([0,100])
ylim([600,1000])
% legend('show','location','southwest','NumColumns',1,'fontsize',10)
% Change text size for presentation
set(gca,'fontsize',16)
box on

subplot(1,2,2)
plot_temperature_AircraftAERIBalloon(n_start,n_end,10,fname_sounding,fname_aeri,fname_aircraft)
xlim([220,240])
ylim([600,1000])
legend('show','location','northwest','NumColumns',1,'fontsize',10)
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_SUP2_25.png'])