classdef  IndexableFunction
%IndexableFunction - A class of function-handle-like objects allowing
%a function to be called and post-indexed in a single expression.
%
%Often, in the MATLAB Central NG, I've seen people ask whether it is
%possible to somehow call functions with a syntax like y=func(arg)(i) as an 
%alternative to doing,
%
%    z=func(arg);
%    y=z(i);
%
%Essentially, the answer is no. Standard MATLAB syntax does not allow this.
%However, the IndexableFunction class enables functions to be called with 
%the very similar syntax y=func{arg}(i). 
%
%If you have to call and post-index a function multiple times, this might 
%save you some keystrokes. However, the main benefit of this submission
%is probably as an illustration/exercise in MATLAB OOP. Also, it
%will give me a link to refer to whenever the discussion point pops up
%again in the NG (as it persistently does). 
%
%In general, there is no computational superiority that this syntax does or
%ever could bestow. Internally, the complete vector-valued output of the
%function is generated and then post-indexed, which is the only generic way  
%of enabling this syntax for an arbitrary set of functions. This is because 
%many MATLAB functions use algorithms that inherently must return a complete 
%vector-valued output (e.g. fft(x)).
%
%USAGE:
%
%  f=IndexableFunction(h)
%
%in:
%
%    h: a function handle
%
%out:
%
%    f: an IndexableFunction object
%
%
%EXAMPLE:
%
%
%         >>h=@sin;  %Handle to sine function
%
%         >>f=IndexableFunction(h);  %Create object
%
%         >>h((0:.25:1)*pi)  %An ordinary kind of function call
% 
%         ans =
% 
%                  0    0.7071    1.0000    0.7071    0.0000
% 
%         >>f{(0:.25:1)*pi}   %Equivalent function call using the object
%                             %(note the braces)
% 
%         ans =
% 
%                  0    0.7071    1.0000    0.7071    0.0000
% 
% 
%         >>f{(0:.25:1)*pi}(3:4)  %The same function call, but returning
%                                 %the 3rd and 4th component only
% 
%         ans =
% 
%             1.0000    0.7071
%
%
%Copyright Jan. 2010, Matt Jacobson, Xoran Technologies, Inc.
%http://www.xorantech.com


    properties
       funcHandle
    end
    methods

        function obj=IndexableFunction(h)
            obj.funcHandle=h;
        end

        function out=subsref(obj,IndexStruct)

            base=obj.funcHandle(IndexStruct(1).subs{:});
            out=base;
            if length(IndexStruct)>1
               out=builtin('subsref',base,IndexStruct(2:end));
            end
        end

        function N=numel(varargin)
            %blablabbla

            N=1;
        end

    end

end
