function plot_temperature_AircraftAERIBalloon(n_start,n_end,dist_limit,fname_sounding,fname_aeri,fname_aircraft)
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
tilt=-70e-3;

for ii=1:length(fname_sounding)
    fname_ii=fname_sounding{ii};
    if length(fname_ii)~=2
        adjustedsounding(fname_ii)
    else
        adjustedsounding(fname_ii{1},1)
    end
end

fname=fname_aircraft;
hour=ncread(fname,'hour');
lat=ncread(fname,'lat');
lon=ncread(fname,'lon');
gps_alt=ncread(fname,'gps_alt');
pres=ncread(fname,'pres');
pres(pres<0)=nan;
vert_wind=ncread(fname,'vert_wind');
vert_wind(vert_wind<-100)=nan;
aircraft_east_vel=ncread(fname,...
    'aircraft_east_vel');
aircraft_north_vel=ncread(fname,...
    'aircraft_north_vel');
lwc_gerber=ncread(fname,'lwc_gerber');
specific_humid=ncread(fname,'specific_humid');
wvmr_dlh=ncread(fname,'wvmr_dlh');
temperature=ncread(fname,'temperature');
equi_potential_temp=ncread(fname,...
    'equi_potential_temp');
rh=ncread(fname,'rh');
% rh=wvmr_dlh/1000./(es(temperature+273.15)./pres/100*18/29);
rho=(pres*100/287./(temperature+273.15));
cond=vert_wind>-100;
cond=lwc_gerber>-0.01;
dist=sqrt((lat-36.605).^2+(lon-97.485).^2)*110;
rs=min(es(temperature+273.15)/100./pres*18/29,wvmr_dlh/1000);
rs(rs<0)=es(temperature(rs<0)+273.15)/100./pres(rs<0)*18/29.*rh(rs<0)/100;
cond=cond&(dist<dist_limit);
trange=zeros(size(hour));
trange(n_start:n_end)=1;
cond=cond&trange;
scatter(temperature(cond)+273.15+tilt*pres(cond),pres(cond),10,'k','filled','DisplayName',...
    ['Aircraft, ',datestr(double(hour(n_start)/24),'HH:MM:SS'),'-',datestr(double(hour(n_end)/24),'HH:MM:SS'),' UTC'])

time_aeri=ncread(fname_aeri,'time');
time_aircraft=hour(cond)*3600;
tts=find((max(time_aircraft)>=time_aeri)&(min(time_aircraft)<=time_aeri));
ll=0;
for tt=tts'
    ll=ll+1;
tdry_aeri=ncread(fname_aeri,'temperature');
tdry_aeri(tdry_aeri<0)=nan;
tdry_aeri=tdry_aeri(:,tt);
tdew_aeri=ncread(fname_aeri,'dewpointTemperature');
tdew_aeri=tdew_aeri(:,tt);
tdew_aeri(tdew_aeri<0)=nan;
pres_aeri=ncread(fname_aeri,'pressure');
pres_aeri=pres_aeri*10; % Convert from kPa to hPa
pres_aeri=pres_aeri(:,tt);
r_aeri=ncread(fname_aeri,'waterVaporMixingRatio')/1000;
r_aeri=r_aeri(:,tt);
rh_aeri=es(tdew_aeri)./es(tdry_aeri);
if ll==1
    plot(tdry_aeri+tilt*pres_aeri,pres_aeri,'--','LineWidth',1,'DisplayName',...
        ['AERI']);
else
    plot(tdry_aeri+tilt*pres_aeri,pres_aeri,'--','LineWidth',1,'HandleVisibility','off');
end
end

function adjustedsounding(fname,varargin)
    
    add24=0;
    if nargin>=2
        add24=varargin{1};
    end
    
    alp=0.272;
    es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
    time=ncread(fname,'base_time');
    tdry=ncread(fname,'tdry');
    tdry=tdry+273.15;
    rh=ncread(fname,'rh_adjust');
    rh(rh==-9999)=nan;
    rh=rh/100;
    pres=ncread(fname,'pres');
    alt=ncread(fname,'alt');
    qv=es(tdry)./pres/100*18/29.*rh;
    equi=equi_pot(tdry,pres,qv);
    tl=(tdry).*(1000./pres).^(287/1004);
    tmix=alp*equi+(1-alp)*tl;
    strtime=split(fname,'.');
    strtime=strtime{4};
    if add24>0
        hp=plot(tdry+tilt*pres,pres,'-','LineWidth',3,'DisplayName',['Balloon, ',strtime(1:2),':',strtime(3:4),':',strtime(5:6),' + 24 UTC']);
    else
        hp=plot(tdry+tilt*pres,pres,'-','LineWidth',3,'DisplayName',['Balloon, ',strtime(1:2),':',strtime(3:4),':',strtime(5:6),'  UTC']);
    end
%     title(ncreadatt(fname,'base_time','string'),'fontsize',20)
    xlabel('Temperature - 0.07 (K/hPa) \cdot pressure (K)')
    ylabel('Pressure (hPa)')
    axis ij
    hold on
end

end