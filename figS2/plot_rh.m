function plot_rh(fname,fname_aeri,varargin)

alp=0.272;
es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
time=ncread(fname,'base_time');
tdry=ncread(fname,'tdry');
tdry=tdry+273.15;
rh=ncread(fname,'rh_adjust');
rh(rh==-9999)=nan;
rh=rh/100;
pres=ncread(fname,'pres');
alt=ncread(fname,'alt');
qv=es(tdry)./pres/100*18/29.*rh;
equi=equi_pot(tdry,pres,qv);
tl=(tdry).*(1000./pres).^(287/1004);
tmix=alp*equi+(1-alp)*tl;
hp=plot(rh*100,pres,'-','LineWidth',3,'DisplayName',['Adjust Sounding at ',ncreadatt(fname,'base_time','string'),' s UTC']);
title(ncreadatt(fname,'base_time','string'),'fontsize',20)
xlabel('Relative Humidity (%)')
ylabel('Pressure (hPa)')
axis ij
hold on

time_aeri=ncread(fname_aeri,'time');
time=ncread(fname,'time');
tts=find((max(time)>=time_aeri)&(min(time)<=time_aeri));
for tt=tts'
tdry_aeri=ncread(fname_aeri,'temperature');
tdry_aeri(tdry_aeri<0)=nan;
tdry_aeri=tdry_aeri(:,tt);
tdew_aeri=ncread(fname_aeri,'dewpointTemperature');
tdew_aeri=tdew_aeri(:,tt);
tdew_aeri(tdew_aeri<0)=nan;
pres_aeri=ncread(fname_aeri,'pressure');
pres_aeri=pres_aeri*10; % Convert from kPa to hPa
pres_aeri=pres_aeri(:,tt);
r_aeri=ncread(fname_aeri,'waterVaporMixingRatio')/1000;
r_aeri=r_aeri(:,tt);
rh_aeri=es(tdew_aeri)./es(tdry_aeri);
plot(rh_aeri*100,pres_aeri,'--','LineWidth',3,'DisplayName',...
    ['AERI profile at ',num2str(time_aeri(tt)),' s UTC']);
end