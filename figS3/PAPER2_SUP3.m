% %% 
% figure
% fname='RACORO_racoro_cf_128_0000006000.nc';
% plot_samples_07122020(fname,[1,51,28,125,83],'k',[307,313,5e-3,11.8e-3]);
% xlim([305,315])
% xlim([300,320])
% ylim([0,15])
% % Change text size for presentation
% set(gca,'fontsize',16)
% box on
% %% print the figure
% print(gcf,'-dpng','-r300',['PAPER2_SUP3.png'])
%% 
figure
fname='RACORO_racoro_cf_128_0000006000.nc';
plot_samples_07122020(fname,[1,31,18,85,70],'k',[307,313,5e-3,11.8e-3]);
xlim([305,315])
xlim([300,320])
ylim([0,15])
% Change text size for presentation
set(gca,'fontsize',16)
box on
%% print the figure
print(gcf,'-dpng','-r300',['PAPER2_SUP3.png'])