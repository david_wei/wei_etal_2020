function [hss,hpp]=plot_samples_07122020(fname,frame,varargin)

cmap=lines;
cmapii=cmap(2,:);
cmap=cmap(2:end,:);
if nargin>2
    if(isstr(varargin{1}))
        cmapii=varargin{1};
    else
        cmapii=cmap(varargin{1},:);
    end
end
    
xyrange=[307,313,4e-3,11.8e-3];
if nargin>3
    if(~isempty(varargin{2}))
        xyrange=varargin{2};
    end
end

es=@(t)611.2*exp(17.67*(t-273)./(t-29.5));
x=ncread(fname,'x');
y=ncread(fname,'y');
z=ncread(fname,'z');
p=ncread(fname,'p');
w=ncread(fname,'W');
qn=ncread(fname,'QN');
qn=qn/1000;
qv=ncread(fname,'QV');
qv=qv/1000;
qt=qn+qv;
tabs=ncread(fname,'TABS');
alpha=0.272;
V_max=10;
bluewhitered

tabsLEG=tabs(frame(1):frame(3),frame(2):frame(4),frame(5));
tabsLEG=tabsLEG(:);
wLEG=w(frame(1):frame(3),frame(2):frame(4),frame(5));
wLEG=wLEG(:);
qvLEG=qv(frame(1):frame(3),frame(2):frame(4),frame(5));
qvLEG=qvLEG(:);
qnLEG=qn(frame(1):frame(3),frame(2):frame(4),frame(5));
qnLEG=qnLEG(:);
qtLEG=qnLEG+qvLEG;
pLEG=p(frame(5));
zLEG=z(frame(5));

% figure
% SAM definition of \theta_{eq} and \theta_l
equiLEG=tabsLEG+(zLEG+318)*9.81/1004+2.5e6/1004*qvLEG;
tmixLEG=alpha*equiLEG+(1-alpha)*(tabsLEG+(zLEG+318)*9.81/1004-...
    2.5e6/1004*qnLEG);

hss=scatter(tmixLEG,qtLEG*1000,40,'filled',...
    'MarkerFaceAlpha',0.2,'MarkerEdgeAlpha',0.2,'DisplayName',['Level ',num2str(varargin{1}),': ',num2str(floor(zLEG)),' m']);
hss.MarkerFaceColor=cmapii;
hss.MarkerEdgeColor=cmapii;
hss.MarkerFaceAlpha=0.6;
hss.MarkerEdgeAlpha=0;
xlim([305,320])
axis ij
hold on

tabsP=squeeze(mean(mean(mean(tabs,1),2),4));
qvP=squeeze(mean(mean(mean(qv,1),2),4));
qnP=squeeze(mean(mean(mean(qn,1),2),4));
equiP=tabsP+(z+318)*9.81/1004+2.5e6/1004*qvP;
hpp=plot(equiP*alpha+(1-alpha)*(tabsP+(z+318)*9.81/1004-2.5e6/1004*qnP),qvP*1000,'r','Handlevisibility','off','Linewidth',2);
plot(equiP(1)*alpha+(1-alpha)*(tabsP(1)+...
    (z(1)+318)*9.81/1004-2.5e6/1004*qnP(1)),qvP(1)*1000,'bo','Handlevisibility','off');
plot(equiP(2)*alpha+(1-alpha)*(tabsP(2)+...
    (z(2)+318)*9.81/1004-2.5e6/1004*qnP(2)),qvP(2)*1000,'go','Handlevisibility','off');

Ttmp=tmixLEG;
qttmp=qtLEG;
plot([xyrange(1),xyrange(1),xyrange(2),xyrange(2),xyrange(1)],...
    [xyrange(3),xyrange(4),xyrange(4),xyrange(3),xyrange(3)]*1000,'Color','k','HandleVisibility','off','Linewidth',2)
condtmp=(Ttmp>=xyrange(1))&(Ttmp<=xyrange(2))&(qttmp>=xyrange(3))&(qttmp<=xyrange(4));
hold on
mdl = fitlm(double(qttmp(condtmp)),double(Ttmp(condtmp)),'linear');
x_lr=(0:1e-4:20e-3)';
[y_lr,dy_lr]=predict(mdl,x_lr);
plot(y_lr,x_lr*1000,'--','Color','k','HandleVisibility','off','Linewidth',2)
patch([dy_lr(:,1);flipud(dy_lr(:,2))]',[x_lr;flipud(x_lr)]'*1000,1,'facecolor','k','edgecolor','none','facealpha',0.3,'HandleVisibility','Off');

    
xlabel([num2str(alpha),'\theta_{e}+',num2str(1-alpha),'\theta_{l} (K)'])
ylabel('q_t (g/kg)')

% colormap(cmap)
cmap=ones(101,3);
cmap(52:101,2)=1-(1:50)/50;
cmap(52:101,3)=1-(1:50)/50;
cmap(50:-1:1,1)=1-(1:50)/50;
cmap(50:-1:1,2)=1-(1:50)/50;
% colormap(cmap)
% hc=colorbar;
% ylabel(hc,'Vertical velocity (m/s)')
% caxis([-V_max,V_max])
box on

plot_profile_06102020(fname,frame,cmapii)

% title(['h = ',num2str(z(frame(5))),'m, ','CB=',num2str(z(find(qnP,1))),...
%     'm, CT=',num2str(z(find(qnP,1,'last'))),'m'])
% % Change text size for presentation
% set(gca,'fontsize',15)